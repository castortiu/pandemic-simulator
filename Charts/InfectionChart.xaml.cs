﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Corona.Annotations;
using LiveCharts;
using LiveCharts.Definitions.Series;
using LiveCharts.Wpf;

namespace Corona.Charts
{
    /// <summary>
    /// Interaction logic for InfectionChart.xaml
    /// </summary>
    public partial class InfectionChart : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private Engine engine;

        private readonly List<ChartParameter> charParameters;

        private readonly Dispatcher dispatcher;
        
        private bool closeRequested;

        public InfectionChart()
        {
            SeriesCollection = new SeriesCollection();

            InitializeComponent();

            charParameters = new List<ChartParameter>
            {
                new ChartParameter(Properties.Resources.InfectedText, engine => (int) engine.Metrics.Infected, engine=>engine.Rules.Infected.Color, InfectedCheck),
                new ChartParameter(Properties.Resources.AsymptomaticText, engine => (int) engine.Metrics.Asymptomatic, engine=>engine.Rules.Asymptomatic.Color, AsymptomaticCheck),
                new ChartParameter(Properties.Resources.SymptomaticText, engine => (int) engine.Metrics.Symptomatic, engine=>engine.Rules.Symptomatic.Color, SymptomaticCheck),
                new ChartParameter(Properties.Resources.ImmunizedText, engine => (int) engine.Metrics.Immunized, engine=>engine.Rules.Immunized.Color, ImmunizedCheck),
                new ChartParameter(Properties.Resources.IsolatedText, engine => (int) engine.Metrics.Isolated, engine=>engine.Rules.Isolated.Color, IsolatedCheck),
                new ChartParameter(Properties.Resources.HospitalizedText, engine => (int) engine.Metrics.Hospitalized, engine=>engine.Rules.Hospitalized.Color, HospitalizedCheck),
                new ChartParameter(Properties.Resources.DeathText, engine => (int) engine.Metrics.Death, engine=>engine.Rules.Death.Color, DiedCheck)
            };

            ChartSetup();

            dispatcher = Application.Current.Dispatcher;
            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }

        public Engine Engine
        {
            get => engine;
            set
            {
                engine = value;
                engine.SimulationCreated += Engine_SimulationCreated;
                engine.DayElapsed += Engine_DayElapsed;
            }
        }

        private void Engine_DayElapsed(object sender, EventArgs e)
        {
            foreach (var chartParameter in charParameters)
                chartParameter.AddCounter(Engine);
        }

        private void Engine_SimulationCreated(object sender, EventArgs e)
        {
            OnUI(() =>
            {
                foreach (var chartParameter in charParameters)
                    chartParameter.Values.Clear();

                BuildSeries();

                AxisSection section = new AxisSection
                {
                    Value = 0, 
                    SectionOffset = 0,
                    SectionWidth = Engine.Rules.Beds,
                    Fill = new SolidColorBrush(Engine.Rules.BedsColor.Color.ChangeOpacity(.5)), 
                    Stroke = new SolidColorBrush(Engine.Rules.BedsColor.Color.ChangeOpacity(.5)), 
                    StrokeThickness = 1
                };

                //AxisSection section1 = new AxisSection
                //{
                //    Value = 10,
                //    SectionWidth = 20,
                //    //SectionOffset = 30,
                //    Fill = new SolidColorBrush(Colors.Purple.ChangeOpacity(.5)),
                //    Stroke = new SolidColorBrush(Colors.Purple.ChangeOpacity(.5)),
                //    StrokeThickness = 5
                //};

                MyChart.AxisY[0].Sections.Add(section);
                MyChart.AxisY[0].Sections.Add(section);

                //MyChart.AxisX[0].Sections.Add(section1);
                //MyChart.AxisX[0].Sections.Add(section1);

            });
        }

        private void BuildSeries()
        {
            if (Engine == null)
                return;

            void SeriesBuilder(Func<Engine, ChartParameter, ISeriesView> action)
            {
                SeriesCollection.Clear();
                foreach (ChartParameter chartParameter in charParameters.Where(p => p.CheckBox.IsChecked == true)) 
                    SeriesCollection.Add(action(Engine, chartParameter));
            }

            if (StakedSeries.IsChecked == true)
                SeriesBuilder(CreateStackedSeries);
            else
                SeriesBuilder(CreateLinearSeries);

            OnPropertyChanged(nameof(SeriesCollection));
        }

        private void OnUI(Action action)
        {
            dispatcher.BeginInvoke(DispatcherPriority.Render, action);
        }

        private void ChartSetup()
        {
            BuildSeries();

            MyChart.AxisY[0].MinValue = 0;
            MyChart.AxisX[0].LabelFormatter = val => "Dia: " + val;
            MyChart.AxisY[0].ShowLabels = true;
        }

        private ISeriesView CreateStackedSeries(Engine chartEngine, ChartParameter charParameter)
        {
            ISeriesView series = new StackedAreaSeries
            {
                Title = charParameter.Key,
                Values = charParameter.Values,
                LineSmoothness = 5,
                StackMode = StackMode.Values,
                Stroke = new SolidColorBrush(charParameter.Color(chartEngine).ChangeLightness(.5)),
                StrokeThickness = 3,
                Fill = new SolidColorBrush(charParameter.Color(chartEngine).ChangeOpacity(.8)),
                PointForeground = new SolidColorBrush(charParameter.Color(engine)),
                PointGeometrySize = 0,
            };
            return series;
        }

        private ISeriesView CreateLinearSeries(Engine chartEngine, ChartParameter charParameter)
        {
            ISeriesView series = new LineSeries
            {
                Title = charParameter.Key,
                Values = charParameter.Values,
                LineSmoothness = 5,
                Stroke = new SolidColorBrush(charParameter.Color(chartEngine).ChangeLightness(.5)),
                StrokeThickness = 3,
                Fill = new SolidColorBrush(charParameter.Color(chartEngine).ChangeOpacity(.5)),
                PointForeground = new SolidColorBrush(charParameter.Color(chartEngine)),
                PointGeometrySize = 0,
            };
            return series;
        }

        public void RequestClose()
        {
            closeRequested = true;
            Close();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (closeRequested == false)
            {
                e.Cancel = true;
                Hide();
            }

            base.OnClosing(e);
        }

        private void SeriesCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            BuildSeries();
        }
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private class ChartParameter
        {
            private readonly Func<Engine, int> counterAction;

            public ChartParameter(string key, Func<Engine, int> counterAction, Func<Engine, Color> colorAction, CheckBox checkBox)
            {
                Key = key;
                this.counterAction = counterAction;
                Values = new ChartValues<int>();
                Color = colorAction;
                CheckBox = checkBox;
            }

            public string Key { get; }

            public ChartValues<int> Values { get; }

            public Func<Engine, Color> Color { get; }

            public CheckBox CheckBox { get; }

            public void AddCounter(Engine engine)
            {
                Values.Add(counterAction(engine));
            }
        }
    }
}
