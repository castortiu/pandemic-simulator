﻿using System.Collections.Generic;
using System.Windows.Media;

namespace Corona
{
    public class Rules
    {
        public Rules()
        {
            SimulationSpeed = 100;
            Population = 100;
            WorldWidth = 500;
            WorldHeight = 300;
            ContagiousRisk = 100;
            IncubationPeriodLow = 8 * 24;
            IncubationPeriodHigh = 12 * 24;
            ImmunizationPeriodLow = 10 * 24;
            ImmunizationPeriodHigh = 15 * 24;
            DeathPeriodLow = 1 * 24;
            DeathPeriodHigh = 2 * 24;
            PopulationRules = new List<PopulationRule>();
            Infected = new IndexedColor(Colors.Red);
            Asymptomatic = new IndexedColor(Colors.Blue);
            Symptomatic = new IndexedColor(Colors.Yellow);
            Immunized = new IndexedColor(Colors.White);
            Isolated = new IndexedColor(Colors.LightGray);
            Hospitalized = new IndexedColor(Colors.Aqua);
            Death = new IndexedColor(Colors.Magenta);
            Healthy = new IndexedColor(Colors.White);
            BedsColor = new IndexedColor(Colors.Pink);
            RespiratorsColor = new IndexedColor(Colors.Brown);
        }

        public int Population { get; set; }

        public int WorldWidth { get; set; }

        public int WorldHeight { get; set; }

        public int SimulationSpeed { get; set; }

        public float ContagiousRisk { get; set; }

        public int IncubationPeriodLow { get; set; }

        public int IncubationPeriodHigh { get; set; }

        public int ImmunizationPeriodLow { get; set; }

        public int ImmunizationPeriodHigh { get; set; }

        public int DeathPeriodLow { get; set; }

        public int DeathPeriodHigh { get; set; }

        public int Beds { get; set; }

        public IndexedColor BedsColor { get; set; }

        public int Respirators { get; set; }

        public IndexedColor RespiratorsColor { get; set; }

        public string Name { get; set; }

        public IndexedColor Infected { get; set; }

        public IndexedColor Asymptomatic { get; set; }

        public IndexedColor Symptomatic { get; set; }

        public IndexedColor Immunized { get; set; }

        public IndexedColor Isolated { get; set; }

        public IndexedColor Hospitalized { get; set; }

        public IndexedColor Healthy { get; set; }

        public IndexedColor Death { get; set; }

        public List<PopulationRule> PopulationRules { get; set; }
    }
}
