﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Corona.Annotations;
using Newtonsoft.Json;

namespace Corona
{
    public class PopulationRule : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private byte bottomAge;
        private byte topAge;
        private byte population = 1;
        private float mortalityRate;
        private byte defaultMobility = 100;
        private byte mobilityMeasure = 100;
        private float symptomaticRisk = 100;
        private IndexedColor color;
        private byte state;

        public PopulationRule(Rules rules)
        {
            Rules = rules;
        }

        public byte BottomAge
        {
            get => bottomAge;
            set
            {
                bottomAge = value;
                OnPropertyChanged(nameof(BottomAge));
            }
        }

        public byte TopAge
        {
            get => topAge;
            set
            {
                topAge = value;
                OnPropertyChanged(nameof(TopAge));
            }
        }

        public byte Population
        {
            get => population;
            set
            {
                population = value;
                OnPropertyChanged(nameof(Population));
            }
        }

        public byte MobilityMeasure
        {
            get => mobilityMeasure;
            set
            {
                mobilityMeasure = value;
                OnPropertyChanged(nameof(MobilityMeasure));
            }
        }

        public float SymptomaticRisk
        {
            get => symptomaticRisk;
            set
            {
                symptomaticRisk = value;
                OnPropertyChanged(nameof(SymptomaticRisk));
            }
        }

        public byte DefaultMobility
        {
            get => defaultMobility;
            set
            {
                defaultMobility = value;
                OnPropertyChanged(nameof(DefaultMobility));
            }
        }

        public float MortalityRate
        {
            get => mortalityRate;
            set
            {
                mortalityRate = value;
                OnPropertyChanged(nameof(MortalityRate));
            }
        }

        public byte State
        {
            get => state;
            set
            {
                state = value;
                OnPropertyChanged(nameof(State));
                if (Rules != null)
                    Color = state.ToColor(Rules);
            }
        }

        public IndexedColor Color 
        { 
            get => color;
            set
            {
                color = value;
                OnPropertyChanged(nameof(Color));
            }
        }

        [JsonIgnore]
        public Rules Rules { get; set; }

        [JsonIgnore]
        public int TotalPopulation { get; set; }

        [JsonIgnore]
        public int CurrentPopulation => TotalPopulation - DeathPopulation;

        [JsonIgnore]
        public int DeathPopulation { get; set; }

        [JsonIgnore] 
        public int ImmunizedPopulation { get; set; }

        [JsonIgnore]
        public byte CurrentMobility { get; private set; }

        public void ResetCounters()
        {
            CurrentMobility = DefaultMobility;
        }

        public void RefreshCounter()
        {
            OnPropertyChanged(nameof(TotalPopulation));
            OnPropertyChanged(nameof(DeathPopulation));
            OnPropertyChanged(nameof(ImmunizedPopulation));
        }

        public PopulationRule Clone()
        {
            PopulationRule newRule = new PopulationRule(Rules)
            {
                Color = Color,
                BottomAge = BottomAge,
                DefaultMobility = DefaultMobility,
                MobilityMeasure = MobilityMeasure,
                MortalityRate = MortalityRate,
                Population = Population,
                TopAge = TopAge,
                TotalPopulation = TotalPopulation,
            };
            return newRule;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
