﻿using System;
using System.Windows.Media;

namespace Corona
{
    public static class Enums
    {
        public static IndexedColor ToColor(this byte state, Rules rules)
        {
            switch (state)
            {
                case Engine.Asymptomatic:
                    return rules.Asymptomatic;
                case Engine.Death:
                    return rules.Death;
                case Engine.Healthy:
                    return rules.Healthy;
                case Engine.Hospitalized:
                    return rules.Hospitalized;
                case Engine.Infected:
                    return rules.Infected;
                case Engine.Isolated:
                    return rules.Isolated;
                case Engine.Immunized:
                    return rules.Immunized;
                case Engine.Symptomatic:
                    return rules.Symptomatic;
                case Engine.Gone:
                    return rules.Death;
            }
            throw new ArgumentException("Invalid State");
        }
    }

    //public enum DiseaseState
    //{
    //    Healthy = 0,
    //    Infected = 1,
    //    Asymptomatic = 2,
    //    Symptomatic = 3,
    //    Isolated = 4,
    //    Hospitalized = 5,
    //    Death = 6,
    //    Immunized = 7
    //}
}
