#include "device_launch_parameters.h"
#include <stdio.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime.h>
#include "structsNative.h"
#include "constants.h"

#define RAND_SCALE 1.0f
#define RAND_SHIFT 0.0f

// Helper functions and utilities to work with CUDA
//#include <helper_functions.h>
//#include <helper_cuda.h>

using namespace Corona;

__device__ void ComputeNextDiseaseState(int kernelThreadId, int currentHour, PersonNative* person);
__device__ inline int rndNext(int kernelThreadId, int maxValue);
__device__ inline float rndNextFloat(int kernelThreadId, float maxValue);
__device__ bool isTrue(int kernelThreadId, float percentage);
__device__ bool isTrue(int kernelThreadId, int currentHour, int fromHour, int lower, int higher);
__device__ void SetPersonState(PersonStateNative state, PersonNative* person, int currentHour);
__device__ inline void computeInfectionState(int kernelThreadId, PersonNative* source, PersonNative* target, int currentHour);

__device__ unsigned int randomIndex = 0;
__device__ curandState* randStates;
__device__ MetricsNative* kernelMetrics;
__device__ float* kernelAnglesX;
__device__ float* kernelAnglesY;
__device__ int* kernelPlayground;
__device__ int kernelPlaygroundWidth;
__device__ int kernelPlaygroundHeight;
__device__ unsigned char* kernelScreen;
__device__ int kernelScreenWidth;
__device__ int kernelScreenHeight;
__device__ PersonNative* kernelPeople;
__device__ int kernelPeopleCount;
__device__ unsigned char* kernelPersonStates;
__device__ unsigned short* kernelRandomAngles;
__device__ int kernelRandomAnglesCount;
__device__ PathogenRulesNative* kernelRules;
__device__ PathogenRulesByAgeNative* kernelRulesByAge;
__device__ int kernelRulesByAgeCount;

__global__ void clearPlaygroundKernel()
{
    int start = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = gridDim.x * blockDim.x;

    for (int idx = start; idx < kernelPlaygroundWidth * kernelPlaygroundHeight; idx += stride)
    {
        kernelPlayground[idx] = -1;
    }
}

__global__ void allocatePeopleKernel()
{
    int start = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = gridDim.x * blockDim.x;

    for (int idx = start; idx < kernelPeopleCount; idx += stride)
    {
        PersonNative* person = kernelPeople + idx;

        if (person->State != PersonStateNative::Healthy)
        {
            switch (person->State)
            {
            case PersonStateNative::Infected:
                atomicAdd(&kernelMetrics->Infected, 1);
                break;
            case PersonStateNative::Asymptomatic:
                atomicAdd(&kernelMetrics->Asymptomatic, 1);
                break;
            case PersonStateNative::Symptomatic:
                atomicAdd(&kernelMetrics->Symptomatic, 1);
                break;
            }
            SetPersonState((PersonStateNative)person->State, person, 0);
        }
        else
        {
            atomicAdd(&kernelMetrics->Healthy, 1);
        }

        if (person->IncrementX < 0.01 && person->IncrementY < 0.01)
        {
            int angleIdx = atomicInc(&randomIndex, kernelRandomAnglesCount);
            unsigned short randomAngle = kernelRandomAngles[angleIdx];
            person->IncrementX = kernelAnglesX[randomAngle] * person->Mobility;
            person->IncrementY = kernelAnglesY[randomAngle] * person->Mobility;
        }

        kernelPlayground[(int)kernelPeople[idx].PositionY * kernelPlaygroundWidth + (int)kernelPeople[idx].PositionX] = idx;
    }
}

__global__ void clearDebugScreenKernel(
    int locationX,
    int locationY)
{
    int start = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = gridDim.x * blockDim.x;

    for (int idx = start; idx < kernelPlaygroundWidth * kernelPlaygroundHeight; idx += stride)
    {
        int personIdx = kernelPlayground[idx];
        if (personIdx != -1)
        {
            PersonNative* person = kernelPeople + personIdx;
            int positionPX = (int)person->PositionX;
            int positionPY = (int)person->PositionY;
            int positionX = (idx % kernelPlaygroundWidth);
            int positionY = (idx / kernelPlaygroundWidth);

            if (positionX != positionPX || positionY != positionPY)
            {
                if (positionX >= 0 && positionX < kernelScreenWidth &&
                    positionY >= 0 && positionY < kernelScreenHeight)
                {
                    positionX -= locationX;
                    positionY -= locationY;
                    kernelScreen[positionY * kernelScreenWidth + positionX] = 255;
                }
            }
        }
    }
}

__device__ void clearKernel(
    int left,
    int top,
    int width,
    int height,
    int locationX,
    int locationY,
    unsigned char color)
{
    for (int y = top - locationY; y < top + height - locationY; y++)
        for (int x = left - locationX; x < left + width - locationX; x++)
        {
            if (x < 0 || x >= kernelScreenWidth ||
                y < 0 || y >= kernelScreenHeight)
                continue;

            kernelScreen[y * kernelScreenWidth + x] = color;
        }
}

__device__ void putMarkers(
    int threadId,
    int blockSize,
    int locationX,
    int locationY,
    unsigned char  color)
{
    if (threadId == 0)
        clearKernel(0, 0, blockSize, blockSize, locationX, locationY, color);
    else if (threadId == 1)
        clearKernel(kernelPlaygroundWidth - blockSize, 0, blockSize, blockSize, locationX, locationY, color);
    else if (threadId == 2)
        clearKernel(0, kernelPlaygroundWidth - blockSize, blockSize, blockSize, locationX, locationY, color);
    else if (threadId == 3)
        clearKernel(kernelPlaygroundWidth - blockSize, kernelPlaygroundHeight - blockSize, blockSize, blockSize, locationX, locationY, color);
}

__global__ void renderScreenKernel(
    int locationX,
    int locationY,
    unsigned char color,
    float zoom)
{
    int blockSize = max(1, (int)(20 / zoom));
    putMarkers(threadIdx.x, blockSize, locationX, locationY, color);
}

__global__ void computeNextMoveKernel(
    int locationX,
    int locationY,
    bool freeze,
    int currentHour)
{
    int start = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = gridDim.x * blockDim.x;

    for (int idx = start; idx < kernelPeopleCount; idx += stride)
    {
        PersonNative* current = kernelPeople + idx;

        if (current->State == PersonStateNative::Gone)
            continue;

        float currentX;
        float currentY;

        if (!freeze)
        {
            currentX = current->PositionX + current->IncrementX;
            currentY = current->PositionY + current->IncrementY;

            if (currentX < 0 || currentX >= kernelPlaygroundWidth)
            {
                current->IncrementX *= -1;
            }
            else if (currentY < 0 || currentY >= kernelPlaygroundHeight)
            {
                current->IncrementY *= -1;
            }
            else
            {
                int currentPosition = (int)current->PositionY * kernelPlaygroundWidth + (int)current->PositionX;
                int newPosition = (int)currentY * kernelPlaygroundWidth + (int)currentX;

                if (currentPosition == newPosition)
                {
                    current->PositionX = currentX;
                    current->PositionY = currentY;
                }
                else
                {
                    int indexNewPosition = atomicCAS(&kernelPlayground[newPosition], -1, idx);
                    if (indexNewPosition == -1)
                    {
                        kernelPlayground[currentPosition] = -1;
                        current->PositionX = currentX;
                        current->PositionY = currentY;
                    }
                    else
                    {
                        int angleIdx = atomicInc(&randomIndex, kernelRandomAnglesCount);
                        unsigned short randomAngle = kernelRandomAngles[angleIdx];
                        current->IncrementX = kernelAnglesX[randomAngle] * current->Mobility;
                        current->IncrementY = kernelAnglesY[randomAngle] * current->Mobility;

                        PersonNative* collisionPerson = kernelPeople + indexNewPosition;
                        if (collisionPerson->State < PersonStateNative::Isolated)
                        {
                            angleIdx = atomicInc(&randomIndex, kernelRandomAnglesCount);
                            randomAngle = kernelRandomAngles[angleIdx];
                            collisionPerson->IncrementX = kernelAnglesX[randomAngle] * collisionPerson->Mobility;
                            collisionPerson->IncrementY = kernelAnglesY[randomAngle] * collisionPerson->Mobility;
                        }

                        computeInfectionState(start, current, collisionPerson, currentHour);
                        computeInfectionState(start, collisionPerson, current, currentHour);
                    }
                }

                ComputeNextDiseaseState(start, currentHour, current);
            }
        }
        else
        {
            currentX = current->PositionX;
            currentY = current->PositionY;
        }

#if !DEBUG_GRID
        int virtualLocationX = (int)currentX - locationX;
        int virtualLocationY = (int)currentY - locationY;
        if (virtualLocationX >= 0 && virtualLocationX < kernelScreenWidth &&
            virtualLocationY >= 0 && virtualLocationY < kernelScreenHeight)
        {
            kernelScreen[virtualLocationY * kernelScreenWidth + virtualLocationX] = current->ColorIndex;
        }
#endif
    }
}

__device__ void ComputeNextDiseaseState(int kernelThreadId, int currentHour, PersonNative* person)
{
    if (person->State == PersonStateNative::Infected)
    {
        bool isHit = isTrue(
            kernelThreadId,
            currentHour,
            person->InfectionHour,
            kernelRules->IncubationPeriodLow,
            kernelRules->IncubationPeriodHigh);
        if (isHit)
        {
            bool isSymptomatic = isTrue(kernelThreadId, kernelRulesByAge[person->Rule].SymptomaticRisk);
            if (isSymptomatic)
            {
                atomicAdd(&kernelMetrics->Symptomatic, 1);
                SetPersonState(PersonStateNative::Symptomatic, person, currentHour);
            }
            else
            {
                atomicAdd(&kernelMetrics->Asymptomatic, 1);
                SetPersonState(PersonStateNative::Asymptomatic, person, currentHour);
            }
        }
    }
    else if (person->State == PersonStateNative::Symptomatic || person->State == PersonStateNative::Asymptomatic)
    {
        bool isHit = isTrue(
            kernelThreadId,
            currentHour,
            person->InfectionHour,
            kernelRules->ImmunizationPeriodLow,
            kernelRules->ImmunizationPeriodHigh);
        if (isHit)
        {
            if (person->State == Asymptomatic)
                atomicSub(&kernelMetrics->Asymptomatic, 1);
            else
                atomicSub(&kernelMetrics->Symptomatic, 1);
            atomicSub(&kernelMetrics->Infected, 1);

            bool died = isTrue(kernelThreadId, kernelRulesByAge[person->Rule].MortalityRate);
            if (died)
            {
                SetPersonState(PersonStateNative::Death, person, currentHour);
                atomicAdd(&kernelMetrics->Death, 1);
                atomicSub(&kernelMetrics->CurrentPopulation, 1);
                person->IncrementX = 0;
                person->IncrementY = 0;
                //                Interlocked.Increment(ref deathThreshold);
                if (kernelMetrics->Infected > 0)
                {
                    kernelMetrics->CurrentMortality = (kernelMetrics->Death / (float)(kernelMetrics->Death + kernelMetrics->Infected + kernelMetrics->Immunized)) * 100;
                    kernelMetrics->MaxMortality = fmaxf(kernelMetrics->MaxMortality, kernelMetrics->CurrentMortality);
                }
            }
            else
            {
                SetPersonState(PersonStateNative::Immunized, person, currentHour);
                atomicAdd(&kernelRulesByAge[person->Rule].Immunized, 1);
                atomicAdd(&kernelMetrics->Immunized, 1);
            }
        }
    }
    else if (person->State == PersonStateNative::Death)
    {
        bool isHit = isTrue(
            kernelThreadId,
            currentHour,
            person->InfectionHour,
            kernelRules->DeathPeriodLow,
            kernelRules->DeathPeriodHigh);
        if (isHit)
        {
            int currentPosition = (int)person->PositionY * kernelPlaygroundWidth + (int)person->PositionX;
            atomicAdd(&kernelRulesByAge[person->Rule].Death, 1);
            atomicExch(&kernelPlayground[currentPosition], -1);
            SetPersonState(PersonStateNative::Gone, person, currentHour);
        }
    }
}

__device__ inline bool isTrue(int kernelThreadId, float percentage)
{
    return percentage >= rndNextFloat(kernelThreadId, 100);
}

__device__ bool isTrue(int kernelThreadId, int currentHour, int fromHour, int lower, int higher)
{
    int current = currentHour - fromHour;

    if (current < lower)
        return false;

    if (current > higher)
        return true;

    double diff = current - lower;
    if (diff >= rndNext(kernelThreadId, higher - lower))
        return true;
    return false;
}

__device__ void SetPersonState(PersonStateNative state, PersonNative* person, int currentHour)
{
    person->State = state;
    person->ColorIndex = kernelPersonStates[state];
    person->InfectionHour = currentHour;
}

__device__ inline void computeInfectionState(int kernelThreadId, PersonNative* source, PersonNative* target, int currentHour)
{
    if ((source->State == PersonStateNative::Symptomatic || source->State == PersonStateNative::Asymptomatic) && target->State == Healthy)
    {
        if (isTrue(kernelThreadId, kernelRules->ContagiousRisk))
        {
            if (atomicCAS(&target->State, PersonStateNative::Healthy, PersonStateNative::Infected) == PersonStateNative::Healthy)
            {
                // Switch to infected
                SetPersonState(PersonStateNative::Infected, target, currentHour);
                atomicAdd(&kernelMetrics->Infected, 1);
                atomicSub(&kernelMetrics->Healthy, 1);
            }
        }
    }
}

__device__ inline int rndNext(int kernelThreadId, int maxValue)
{
    return (int)(curand_uniform(&randStates[kernelThreadId]) * maxValue);
}

__device__ inline float rndNextFloat(int kernelThreadId, float maxValue)
{
    return curand_uniform(&randStates[kernelThreadId]) * maxValue;
}

__global__ void setupKernelEverytime(
    int* playground,
    int playgroundWidth,
    int playgroundHeight,
    unsigned char* screen,
    int screenWidth,
    int screenHeight,
    PersonNative* people,
    int peopleCount,
    unsigned char* personStates,
    unsigned short* randomAngles,
    int randomAnglesCount,
    PathogenRulesNative* rules,
    PathogenRulesByAgeNative* rulesByAge,
    int rulesByAgeCount)
{
    kernelPlayground = playground;
    kernelPlaygroundWidth = playgroundWidth;
    kernelPlaygroundHeight = playgroundHeight;
    kernelScreen = screen;
    kernelScreenWidth = screenWidth;
    kernelScreenHeight = screenHeight;
    kernelPeople = people;
    kernelPeopleCount = peopleCount;
    kernelPersonStates = personStates;
    kernelRandomAngles = randomAngles;
    kernelRandomAnglesCount = randomAnglesCount;
    kernelRules = rules;
    kernelRulesByAge = rulesByAge;
    kernelRulesByAgeCount = rulesByAgeCount;
}

__global__ void setupKernelOnce(
    curandState* state,
    MetricsNative* metrics,
    float* anglesX,
    float* anglesY)
{
    randStates = state;
    kernelMetrics = metrics;
    kernelAnglesX = anglesX;
    kernelAnglesY = anglesY;
}

__global__ void setupKernelMulti(
    unsigned long seed)
{
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(seed, id, 0, &randStates[id]);
}