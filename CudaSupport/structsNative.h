#pragma once
#ifndef _structsNative_H_
#define _structsNative_H_

namespace Corona
{
    typedef struct
    {
        unsigned int CurrentHour;
        unsigned int TotalPopulation;
        unsigned int CurrentPopulation;
        unsigned int Healthy;
        unsigned int Infected;
        unsigned int Asymptomatic;
        unsigned int Symptomatic;
        unsigned int Immunized;
        unsigned int Isolated; 
        unsigned int Hospitalized;
        unsigned int Death; 
        float CurrentMortality;
        float MaxMortality;
    } MetricsNative;

    typedef struct
    {
        float PositionX; 
        float PositionY;
        float IncrementX;
        float IncrementY;
        float Mobility;
        int State;
        unsigned int InfectionHour;
        unsigned char ColorIndex;
        unsigned char Rule;
    } PersonNative;

    typedef struct
    {
        float ContagiousRisk;
        int IncubationPeriodHigh;
        int IncubationPeriodLow;
        int ImmunizationPeriodLow;
        int ImmunizationPeriodHigh;
        int DeathPeriodLow;
        int DeathPeriodHigh;
    } PathogenRulesNative;

    typedef struct
    {
        unsigned char BottomAge;
        unsigned char TopAge;
        float MortalityRate;
        float RegularMobility;
        float SymptomaticRisk;
        int Death;
        int Immunized;
    } PathogenRulesByAgeNative;
}
#endif