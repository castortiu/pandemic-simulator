#pragma once
#ifndef _constantsManaged_H_
#define _constantsManaged_H_

namespace Corona
{
    public enum class PersonState
    {
        Healthy = 0,
        Infected = 1,
        Asymptomatic = 2,
        Symptomatic = 3,
        Immunized = 4,
        Isolated = 5,
        Hospitalized = 6,
        Death = 7
    };
}

#endif

