#pragma once

#pragma once
#ifdef _MANAGED

namespace Corona
{
    struct DefaultDelegate;

    template<typename... Args>
    value struct DelegateType;

    template<typename Ret, typename... Args>
    value struct DelegateType<DefaultDelegate, Ret, Args...>
    {
        delegate Ret MyDelegate(Args...);
        typedef MyDelegate delegate_type;
    };

    template<typename Target, typename Ret, typename... Args>
    value struct DelegateType<Target, Ret, Args...>
    {
        typedef Target delegate_type;
    };

    template<typename Lambda>
    ref class LambdaWrapper
    {
    public:
        LambdaWrapper(Lambda&& lambda) : func(new Lambda(std::forward<Lambda>(lambda))) {}
        !LambdaWrapper() { delete func; }
        ~LambdaWrapper() { delete func; }
        template<typename Ret, typename... Args>
        Ret CallLambda(Args... args) { return (*func)(args...); }
    private:
        Lambda* func;
    };

    template<typename Target, typename Lambda, typename Ret, typename... Args>
    auto _toDelegate(Lambda&& lambda, Ret(Lambda::* func)(Args...))
    {
        LambdaWrapper<Lambda>^ lw = gcnew LambdaWrapper<Lambda>(std::forward<Lambda>(lambda));
        return gcnew typename DelegateType<Target, Ret, Args...>::delegate_type(lw, &LambdaWrapper<Lambda>::CallLambda<Ret, Args...>);
    }

    template<typename Target, typename Lambda, typename Ret, typename... Args>
    auto _toDelegate(Lambda&& lambda, Ret(Lambda::* func)(Args...) const)
    {
        LambdaWrapper<Lambda>^ lw = gcnew LambdaWrapper<Lambda>(std::forward<Lambda>(lambda));
        return gcnew typename DelegateType<Target, Ret, Args...>::delegate_type(lw, &LambdaWrapper<Lambda>::CallLambda<Ret, Args...>);
    }

    template<typename Target, typename Lambda>
    auto toDelegate(Lambda&& lambda)
    {
        return _toDelegate<Target>(std::forward<Lambda>(lambda), &Lambda::operator());
    }
}
#endif