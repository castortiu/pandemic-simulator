#pragma once
#ifndef _LambdasHelpers_
#define _LambdasHelpers_

using namespace System;

namespace Corona
{
    public ref class GetRandomAngleHelper
    {
    public:
        GetRandomAngleHelper(Random^ rnd)
        {
            random = rnd;
        }

        static Random^ random;

        static short GetRandom(int i)
        {
            return (short)random->Next(360);
        }
    };
}

#endif
