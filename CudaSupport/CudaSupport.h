#pragma once
#ifndef _CudaSupport_H_
#define _CudaSupport_H_

#include "CudaException.cpp"
#include "CudaSupportNative.cpp"

namespace Corona
{
    public ref class CudaSupport : IDisposable
    {
    private:
        CudaSupportNative* cuda;
        Metrics^ metrics;
        array<PathogenRulesByAge>^ pathogenRulesByAge;

    public:
        CudaSupport() : cuda(new CudaSupportNative)
        {
        }

        ~CudaSupport()
        {
            this->!CudaSupport();
        }

        property int PlaygroundWidth
        {
            int get()
            {
                return cuda->playgroundWidth;
            }
        }

        property int PlaygroundHeight
        {
            int get()
            {
                return cuda->playgroundHeight;
            }
        }

        property int ScreenWidth
        {
            int get()
            {
                return cuda->screenWidth;
            }
        }

        property int ScreenHeight
        {
            int get()
            {
                return cuda->screenHeight;
            }
        }

        property int PeopleCount
        {
            int get()
            {
                return cuda->peopleCount;
            }
        }

        void AllocatePlayground(int width, int height, PathogenRules^ rules, array<PathogenRulesByAge>^ rulesByAge);
        void AllocateScreen(int width, int height);
        void AllocateStates(array<unsigned char>^ stateColors);
        void AllocatePeople(array<Person>^ people);
        void ComputeNextMove(int viewportLeft, int viewportTop, bool freeze);
        void TransferScreenToHost(unsigned char* hostScreen);
        void RenderMarkers(int viewportLeft, int viewportTop, unsigned char color, float zoom);
        void AllocateMetrics();

        Metrics GetMetrics();
        array<PathogenRulesByAge>^ GetRulesMetrics();
        Person GetPatientZero();
        void TransferPeopleToDevice();
        void Free();

    private:


        void ThrowIfError()
        {
            if (cuda->lastCudaError != cudaSuccess)
            {
                throw gcnew CudaException(gcnew String(cuda->szLastError.c_str()));
            }
        }

    protected:
        !CudaSupport()
        {
            delete cuda;
        }
    };
}

#endif
