#pragma once
#ifndef _constants_H_
#define _constants_H_

namespace Corona
{
    enum PersonStateNative
    {
        Healthy = 0,
        Infected = 1,
        Asymptomatic = 2,
        Symptomatic = 3,
        Immunized = 4,
        Isolated = 5,
        Hospitalized = 6,
        Death = 7,
        Gone = 8
    };
}

#endif