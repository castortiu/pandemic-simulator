#pragma once
#ifndef _structsManaged_H_
#define _structsManaged_H_

namespace Corona
{
    public value struct Metrics
    {
        property unsigned int CurrentHour;
        property unsigned int TotalPopulation;
        property unsigned int CurrentPopulation;
        property unsigned int Healthy;
        property unsigned int Infected;
        property unsigned int Asymptomatic;
        property unsigned int Symptomatic;
        property unsigned int Immunized;
        property unsigned int Isolated;
        property unsigned int Hospitalized;
        property unsigned int Death;
        property float CurrentMortality;
        property float MaxMortality;
    };

    public value struct Person
    {
        Person(unsigned char state, unsigned char rule)
        {
            PositionX = 0;
            PositionY = 0;
            IncrementX = 0;
            IncrementY = 0;
            ColorIndex = 255;
            Mobility = 1;
            State = state;
            Rule = rule;
        }

        float PositionX;
        float PositionY;
        float IncrementX;
        float IncrementY;
        float Mobility;
        int State;
        unsigned int InfectionHour;
        unsigned char ColorIndex;
        unsigned char Rule;
    };

    public value struct PathogenRules
    {
        float ContagiousRisk;
        int IncubationPeriodHigh;
        int IncubationPeriodLow;
        int ImmunizationPeriodLow;
        int ImmunizationPeriodHigh;
        int DeathPeriodLow;
        int DeathPeriodHigh;
    };

    public value struct PathogenRulesByAge
    {
        unsigned char BottomAge;
        unsigned char TopAge;
        float MortalityRate;
        float RegularMobility;
        float SymptomaticRisk;
        int Death;
        int Immunized;
    };
}
#endif