#pragma once
#ifndef _CudaSupportNative_
#define _CudaSupportNative_

#define _USE_MATH_DEFINES
#include <math.h>
#include "structsNative.h"
#include <curand.h>
#include <curand_kernel.h>

extern void clearPlaygroundKernel();
extern void allocatePeopleKernel();
extern void clearDebugScreenKernel(
    int locationX,
    int locationY);
extern void computeNextMoveKernel(
    int locationX,
    int locationY,
    bool freeze,
    int currentHour);
extern void renderScreenKernel(
    int locationX,
    int locationY,
    unsigned char color,
    float zoom);
extern void setupKernelOnce(
    curandState* state,
    Corona::MetricsNative* metrics,
    float* anglesX,
    float* anglesY);
extern void setupKernelEverytime(
    int* playground,
    int playgroundWidth,
    int playgroundHeight,
    unsigned char* screen,
    int screenWidth,
    int screenHeight,
    Corona::PersonNative* people,
    int peopleCount,
    unsigned char* personStates,
    unsigned short* randomAngles,
    int randomAnglesCount,
    Corona::PathogenRulesNative* rules,
    Corona::PathogenRulesByAgeNative* rulesByAge,
    int rulesByAgeCount);
extern void setupKernelMulti(
    unsigned long seed);
#endif
