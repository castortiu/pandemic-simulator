#include "pch.h"
#include "CudaSupport.h"
#include "LamdasHelpers.h"
#include <time.h>
#include "constantsManaged.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;
using namespace System::Linq;

namespace Corona
{
    void CudaSupport::AllocatePlayground(int width, int height, PathogenRules^ rules, array<PathogenRulesByAge>^ rulesByAge)
    {
        pin_ptr<PathogenRules> rulesPtr = &*rules;
        PathogenRulesNative* nativeRulesPtr = (PathogenRulesNative*)rulesPtr;

        pathogenRulesByAge = rulesByAge;

        cuda->AllocatePlayground(width, height, *nativeRulesPtr, rulesByAge);
        ThrowIfError();

        cuda->ClearPlayground();
        ThrowIfError();
    }

    void CudaSupport::AllocateScreen(int width, int height)
    {
        cuda->AllocateScreen(width, height);
        ThrowIfError();
    }

    void CudaSupport::AllocateStates(array<unsigned char>^ stateColors)
    {
        cuda->AllocateStates(stateColors);
        ThrowIfError();
    }

    void CudaSupport::AllocatePeople(array<Person>^ people)
    {
        cuda->AllocatePeople(people);
        ThrowIfError();
    }

    void CudaSupport::ComputeNextMove(int viewportLeft, int viewportTop, bool freeze)
    {
        cuda->ComputeNextMove(viewportLeft, viewportTop, freeze, metrics);
        ThrowIfError();
    }

    void CudaSupport::TransferScreenToHost(unsigned char* hostScreen)
    {
        cuda->TransferScreenToHost(hostScreen);
        ThrowIfError();
    }

    void CudaSupport::TransferPeopleToDevice()
    {
        cuda->TransferPeopleToDevice();
        ThrowIfError();
    }

    void CudaSupport::RenderMarkers(int viewportLeft, int viewportTop, unsigned char color, float zoom)
    {
        cuda->RenderMarkers(viewportLeft, viewportTop, color, zoom);
        ThrowIfError();
    }

    void CudaSupport::AllocateMetrics()
    {
        metrics = gcnew Metrics();
        metrics->TotalPopulation = cuda->peopleCount;
        metrics->CurrentPopulation = cuda->peopleCount;
        cuda->InitializeMetrics(metrics);
        ThrowIfError();
    }

    Metrics CudaSupport::GetMetrics()
    {
        cuda->GetMetrics(metrics);
        return *metrics;
    }

    array<PathogenRulesByAge>^ CudaSupport::GetRulesMetrics()
    {
        cuda->GetRulesMetrics(pathogenRulesByAge);
        return pathogenRulesByAge;
    }

    Person CudaSupport::GetPatientZero()
    {
        PersonNative personNative;
        Person person;
        cuda->GetPatientZero(&personNative);
        memcpy(&person, &personNative, sizeof(personNative));
        ThrowIfError();

        return person;
    }

    void CudaSupport::Free()
    {
        cuda->Free();
    }
}
