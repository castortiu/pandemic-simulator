﻿#include "pch.h"
#include "CudaSupportNative.h"
#include <functional>
#include <string>
#include <random>

#include "constants.h"
#include "cuda_runtime.h"
#include "structsManaged.h"
#include <curand.h>
#include <curand_kernel.h>
#include <time.h>
#include <iostream>

#define CUDA_THREADS 256
#define CUDA_BLOCKS 32

using namespace System;
using namespace System::Diagnostics;

namespace Corona
{
    private class CudaSupportNative
    {
    private:
        int* playground;
        int* screen;
        float* anglesX;
        float* anglesY;
        unsigned short* randomAngles;
        int randomAnglesCount;
        unsigned char* personStates;
        PersonNative* people;
        MetricsNative* metrics;
        curandState* devStates;
        PathogenRulesNative* rules;
        PathogenRulesByAgeNative* rulesByAge;
        int rulesByAgeCount;
        int patientZero;

    public:
        int playgroundWidth;
        int playgroundHeight;
        int screenWidth;
        int screenHeight;
        int peopleCount;

        cudaError_t lastCudaError;
        std::wstring szLastError;

        CudaSupportNative()
        {
            AllocateMetrics();

            AllocateRandomStates();

            AllocateAngles();

            void* argsOnce[] = { &devStates, &metrics, &anglesX, &anglesY };
            if (cudaFailed(cudaLaunchKernel((void const*)&setupKernelOnce, dim3(1), dim3(1), argsOnce),
                []() { return L"Failed setup kernel Once"; }))
                return;

            time_t currentTime = time(NULL);
            void* args[] = { &currentTime, &devStates, &metrics };
            if (cudaFailed(cudaLaunchKernel((void const*)&setupKernelMulti, dim3(CUDA_BLOCKS), dim3(CUDA_THREADS), args),
                []() { return L"Failed setup kernel Multi"; }))
                return;
        }

        cudaError_t TransferPointersHostToDevice()
        {
            void* argsEverytime[] = {
                &playground,
                &playgroundWidth,
                &playgroundHeight,
                &screen,
                &screenWidth,
                &screenHeight,
                &people,
                &peopleCount,
                &personStates,
                &randomAngles,
                &randomAnglesCount,
                &rules,
                &rulesByAge,
                &rulesByAgeCount };
            if (cudaFailed(cudaLaunchKernel((void const*)&setupKernelEverytime, dim3(1), dim3(1), argsEverytime),
                []() { return L"Failed setup kernel Everytime"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t GetPatientZero(PersonNative* personNative)
        {
            if (patientZero == -1)
            {
                return Success();
            }

            if (cudaFailed(cudaMemcpy(personNative, people + patientZero, sizeof(PersonNative), cudaMemcpyKind::cudaMemcpyDeviceToHost),
                []() { return L"Failed to copy PatienZero"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t AllocatePlayground(
            int width,
            int height,
            PathogenRulesNative pathogenRules,
            array<PathogenRulesByAge>^ pathogenRulesByAge)
        {
            if (SetDevice(0) != cudaSuccess)
                goto Error;

            cudaFree(playground);
            if (cudaFailed(cudaMalloc((void**)&playground, width * height * sizeof(int)),
                []() { return L"Playground allocation on device failed"; }))
                goto Error;

            playgroundWidth = width;
            playgroundHeight = height;

            cudaFree(rules);
            if (cudaFailed(cudaMalloc((void**)&rules, sizeof(PathogenRulesNative)),
                []() { return L"Rules allocation on device failed"; }))
                goto Error;

            if (cudaFailed(cudaMemcpy(rules, &pathogenRules, sizeof(PathogenRulesNative), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"Rules copy on device failed"; }))
                goto Error;

            cudaFree(rulesByAge);
            if (cudaFailed(cudaMalloc((void**)&rulesByAge, sizeof(PathogenRulesByAgeNative) * pathogenRulesByAge->Length),
                []() { return L"Rules by age allocation on device failed"; }))
                goto Error;

            pin_ptr<PathogenRulesByAge> rulesByAgeNative = &pathogenRulesByAge[0];
            if (cudaFailed(cudaMemcpy(rulesByAge, rulesByAgeNative, sizeof(PathogenRulesByAgeNative) * pathogenRulesByAge->Length, cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"Rules by age copy on device failed"; }))
                goto Error;

            TransferPointersHostToDevice();

            AllocateRandomAngles();

            return Success();

        Error:
            cudaFree(playground);
            cudaFree(rules);
            cudaFree(rulesByAge);
            return lastCudaError;
        }

        cudaError_t InitializeMetrics(Metrics^ currentMetrics)
        {
            pin_ptr<Metrics> nativePtr = &*currentMetrics;
            if (cudaFailed(cudaMemcpy(metrics, nativePtr, sizeof(MetricsNative), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"Failed to copy metrics to device"; }))
                goto Error;

            return Success();

        Error:
            return lastCudaError;
        }

        cudaError_t AllocateScreen(int width, int height)
        {
            if (SetDevice(0) != cudaSuccess)
                return lastCudaError;

            cudaFree(screen);
            if (cudaFailed(cudaMalloc((void**)&screen, width * height * sizeof(unsigned char)),
                []() { return L"Screen allocation on device failed"; }))
                return lastCudaError;

            screenWidth = width;
            screenHeight = height;

            TransferPointersHostToDevice();

            return Success();
        }

        cudaError_t AllocateRandomAngles()
        {
            int size = 2000;
            unsigned short randoms[2000];

            short lower_bound = 0;
            short upper_bound = 360;
            std::uniform_int_distribution<short> unif(lower_bound, upper_bound);
            std::default_random_engine re;
            short a_random_double = unif(re);

            for (int idx = 0; idx < size; idx++)
            {
                randoms[idx] = unif(re);
            }

            if (cudaFailed(cudaMalloc((void**)&randomAngles, size * sizeof(unsigned short)),
                []() { return L"Random number array allocation on device failed"; }))
                goto Error;

            if (cudaFailed(cudaMemcpy(randomAngles, randoms, size * sizeof(unsigned short), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"Random number array copy on device failed"; }))
                goto Error;

            randomAnglesCount = size;

            TransferPointersHostToDevice();

            return Success();

        Error:
            cudaFree(randomAngles);
            return lastCudaError;
        }

        cudaError_t AllocateStates(array<unsigned char>^ stateColors)
        {
            if (cudaFailed(cudaMalloc((void**)&personStates, stateColors->Length * sizeof(unsigned char)),
                []() { return L"States array allocation on device failed"; }))
                goto Error;

            pin_ptr<unsigned char> statesPtr = &stateColors[0];
            if (cudaFailed(cudaMemcpy(personStates, statesPtr, stateColors->Length * sizeof(unsigned char), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"States array copy on device failed"; }))
                goto Error;

            TransferPointersHostToDevice();

            return Success();

        Error:
            cudaFree(personStates);
            return lastCudaError;
        }

        cudaError_t AllocatePeople(array<Person>^ peopleArray)
        {
            if (cudaFailed(cudaMalloc((void**)&people, peopleArray->Length * sizeof(PersonNative)),
                []() { return L"People array allocation on device failed"; }))
                goto Error;

            pin_ptr<Person> peoplePtr = &peopleArray[0];
            if (cudaFailed(cudaMemcpy(people, peoplePtr, peopleArray->Length * sizeof(PersonNative), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"People array copy on device failed"; }))
                goto Error;

            peopleCount = peopleArray->Length;

            for (int idx = 0; idx < peopleArray->Length; idx++)
            {
                if (peopleArray[idx].State != (unsigned char)PersonStateNative::Healthy)
                {
                    patientZero = idx;
                    break;
                }
            }

            TransferPointersHostToDevice();

            return Success();

        Error:
            cudaFree(people);
            return lastCudaError;
        }

        cudaError_t ClearPlayground()
        {
            if (cudaFailed(cudaLaunchKernel((void const*)&clearPlaygroundKernel, dim3(32), dim3(512), NULL),
                []() { return L"Failed to clear they playground"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t TransferPeopleToDevice()
        {
            if (cudaFailed(cudaLaunchKernel((void const*)&allocatePeopleKernel, dim3(32), dim3(512), NULL),
                []() { return L"Failed to allocate people in the kernel"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t GetMetrics(Metrics^ currentMetrics)
        {
            pin_ptr<Metrics> nativePtr = &*currentMetrics;
            unsigned char* hostMetrics = (unsigned char*)nativePtr;
            unsigned char* deviceMetrics = (unsigned char*)metrics;
            if (cudaFailed(cudaMemcpy(hostMetrics + sizeof(unsigned int), deviceMetrics + sizeof(unsigned int), sizeof(MetricsNative) - sizeof(unsigned int), cudaMemcpyKind::cudaMemcpyDeviceToHost),
                []() { return L"Failed to copy metrics memory to host"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t GetRulesMetrics(array<PathogenRulesByAge>^ pathogenRulesByAge)
        {
            pin_ptr<PathogenRulesByAge> rulesByAgeNative = &pathogenRulesByAge[0];
            if (cudaFailed(cudaMemcpy(rulesByAgeNative, rulesByAge, sizeof(PathogenRulesByAgeNative) * pathogenRulesByAge->Length, cudaMemcpyKind::cudaMemcpyDeviceToHost),
                []() { return L"Rules by age copy on host failed"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t ComputeNextMove(int viewportLeft, int viewportTop, bool freeze, Metrics^ currentMetrics)
        {
            currentMetrics->CurrentHour += 1;

            if (cudaFailed(cudaMemset(screen, 0, sizeof(unsigned char) * screenWidth * screenHeight),
                []() { return L"Failed to clear the screen"; }))
                return lastCudaError;

            int currentHour = currentMetrics->CurrentHour;
            void* args1[] = { &viewportLeft, &viewportTop, &freeze, &currentHour };
            if (cudaFailed(cudaLaunchKernel((void const*)&computeNextMoveKernel, dim3(32), dim3(256), args1),
                []() { return L"Failed to compute next move the screen"; }))
                return lastCudaError;

#if DEBUG_GRID
            if (cudaFailed(cudaDeviceSynchronize(),
                []() { return L"Failed to synchronize device"; }))
                return lastCudaError;

            void* args[] = { &viewportLeft, &viewportTop };
            if (cudaFailed(cudaLaunchKernel((void const*)&clearDebugScreenKernel, dim3(32), dim3(256), args),
                []() { return L"Failed to debug the screen"; }))
                return lastCudaError;
#endif 

            if (cudaFailed(cudaDeviceSynchronize(),
                []() { return L"Failed to synchronize device"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t RenderMarkers(int viewportLeft, int viewportTop, unsigned char color, float zoom)
        {
            void* args[] = { &viewportLeft, &viewportTop, &color, &zoom };
            if (cudaFailed(cudaLaunchKernel((void const*)&renderScreenKernel, dim3(1), dim3(4), args),
                []() { return L"Failed to put markers on the screen"; }))
                return lastCudaError;

            return Success();
        }

        void Free()
        {
            cudaFree(playground);
            playground = NULL;
            cudaFree(screen);
            screen = NULL;
            cudaFree(randomAngles);
            randomAngles = NULL;
            cudaFree(personStates);
            personStates = NULL;
            cudaFree(people);
            people = NULL;
            cudaFree(rules);
            rules = NULL;
            cudaFree(rulesByAge);
            rulesByAge = NULL;
            playgroundWidth = 0;
            playgroundHeight = 0;
            screenWidth = 0;
            screenHeight = 0;
            randomAnglesCount = 0;
            peopleCount = 0;
        }

        cudaError_t TransferScreenToHost(unsigned char* hostScreen)
        {
            if (screen != nullptr)
            {
                if (cudaFailed(cudaMemcpy(hostScreen, screen, sizeof(unsigned char) * screenWidth * screenHeight, cudaMemcpyKind::cudaMemcpyDeviceToHost),
                    []() { return L"Failed to copy screen memory to host"; }))
                    return lastCudaError;
            }

            return Success();
        }

        ~CudaSupportNative()
        {
            Free();
            cudaFree(metrics);
            metrics = NULL;
            cudaFree(devStates);
            devStates = NULL;
            cudaFree(anglesX);
            anglesX = NULL;
            cudaFree(anglesY);
            anglesY = NULL;
            cudaFailed(cudaDeviceReset(), []() { return L"cudaDeviceReset failed!"; });
        }

    private:
        void gpuErrchk(cudaError_t code)
        {
            gpuAssert(code, __FILE__, __LINE__);
        }

        inline void gpuAssert(cudaError_t code, char* file, int line, bool abort = true)
        {
            if (code != cudaSuccess)
            {
                fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
                if (abort)
                {
                    exit(code);
                }
            }
        }

        cudaError_t AllocateMetrics()
        {
            if (cudaFailed(cudaMalloc((void**)&metrics, sizeof(MetricsNative)),
                []() { return L"Metrics allocation on device failed"; }))
                return lastCudaError;

            return Success();
        }

        cudaError_t AllocateRandomStates()
        {
            gpuErrchk(cudaMalloc((void**)&devStates, CUDA_BLOCKS * CUDA_THREADS * sizeof(curandState)));

            return Success();
        }

        cudaError_t AllocateAngles()
        {
            // Initialize angle vectors to gain perf
            float anglesXArray[360];
            float anglesYArray[360];
            float step = (float)M_PI * 2 / 360.0f;
            for (int idx = 0; idx < 360; idx++)
            {
                double angle = idx * step;
                anglesXArray[idx] = (float)sin(angle);
                anglesYArray[idx] = (float)cos(angle);
            }

            if (cudaFailed(cudaMalloc((void**)&anglesX, 360 * sizeof(float)),
                []() { return L"AnglesX allocation on device failed"; }))
                goto Error;

            if (cudaFailed(cudaMemcpy(anglesX, anglesXArray, 360 * sizeof(float), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"AnglesX copy on device failed"; }))
                goto Error;

            if (cudaFailed(cudaMalloc((void**)&anglesY, 360 * sizeof(float)),
                []() { return L"AnglesY allocation on device failed"; }))
                goto Error;

            if (cudaFailed(cudaMemcpy(anglesY, anglesYArray, 360 * sizeof(float), cudaMemcpyKind::cudaMemcpyHostToDevice),
                []() { return L"AnglesY copy on device failed"; }))
                goto Error;

            return Success();

        Error:
            cudaFree(anglesX);
            cudaFree(anglesY);
            return lastCudaError;
        }

        cudaError_t Success()
        {
            lastCudaError = cudaSuccess;
            return cudaSuccess;
        }

        cudaError_t cudaFailed(cudaError_t cudaStatus, std::function<std::wstring()> errorMessage)
        {
            if (cudaStatus != cudaSuccess)
            {
                lastCudaError = cudaStatus;
                szLastError = errorMessage();
            }
            return cudaStatus;
        }

        cudaError_t SetDevice(int device)
        {
            // Choose which GPU to run on, change this on a multi-GPU system.
            return cudaFailed(cudaSetDevice(0), []() { return L"cudaSetDevice failed! Do you have a CUDA-capable GPU installed?"; });
        }

#define cudaSafeCall(call)  \
        do {\
            cudaError_t err = call;\
            if (cudaSuccess != err) \
            {\
                std::cerr << "CUDA error in " << __FILE__ << "(" << __LINE__ << "): " \
                    << cudaGetErrorString(err);\
                exit(EXIT_FAILURE);\
            }\
        } while(0)

        //int MyMethod()
        //{
        //    //Console::WriteLine(L"Hello World");

        //    int const arraySize = 5;
        //    int const a[arraySize] = { 1, 2, 3, 4, 5 };
        //    int const b[arraySize] = { 10, 20, 30, 40, 50 };
        //    int c[arraySize] = { 0 };

        //    // Add vectors in parallel.
        //    cudaError_t cudaStatus = addWithCuda(c, a, b, arraySize);
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"addWithCuda failed!");
        //        return 1;
        //    }

        //    Console::WriteLine(L"[1,2,3,4,5] + [10,20,30,40,50] = {0},{1},{2},{3},{4}\n",
        //        c[0], c[1], c[2], c[3], c[4]);

        //    // cudaDeviceReset must be called before exiting in order for profiling and
        //    // tracing tools such as Nsight and Visual Profiler to show complete traces.
        //    cudaStatus = cudaDeviceReset();
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaDeviceReset failed!");
        //        return 1;
        //    }

        //    return 0;
        //}

        //cudaError_t addWithCuda(int* c, int const* a, int const* b, unsigned int size)
        //{
        //    extern void addKernel(int* c, int const* a, const int* b);

        //    int* dev_a = 0;
        //    int* dev_b = 0;
        //    int* dev_c = 0;
        //    cudaError_t cudaStatus;

        //    // Choose which GPU to run on, change this on a multi-GPU system.
        //    cudaStatus = cudaSetDevice(0);
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaSetDevice failed! Do you have a CUDA-capable GPU installed?");
        //        goto Error;
        //    }

        //    // Allocate GPU buffers for three vectors (two input, one output)    .
        //    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaMalloc failed!");
        //        goto Error;
        //    }

        //    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaMalloc failed!");
        //        goto Error;
        //    }

        //    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaMalloc failed!");
        //        goto Error;
        //    }

        //    // Copy input vectors from host memory to GPU buffers.
        //    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaMemcpy failed!");
        //        goto Error;
        //    }

        //    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaMemcpy failed!");
        //        goto Error;
        //    }

        //    // Launch a kernel on the GPU with one thread for each element.
        //    //  replace addKernel<<<1, size>>>(dev_c, dev_a, dev_b); with:
        //    void* args[] = { &dev_c, &dev_a, &dev_b };
        //    cudaStatus = cudaLaunchKernel(
        //        (void const*)&addKernel, // pointer to kernel func.
        //        dim3(1), // grid
        //        dim3(size), // block
        //        args  // arguments
        //    );

        //    // Check for any errors launching the kernel
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"addKernel launch failed: {0}\n", gcnew String(cudaGetErrorString(cudaStatus)));
        //        goto Error;
        //    }

        //    // cudaDeviceSynchronize waits for the kernel to finish, and returns
        //    // any errors encountered during the launch.
        //    cudaStatus = cudaDeviceSynchronize();
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaDeviceSynchronize returned error code {0} after launching addKernel!\n", (int)cudaStatus);
        //        goto Error;
        //    }

        //    // Copy output vector from GPU buffer to host memory.
        //    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
        //    if (cudaStatus != cudaSuccess)
        //    {
        //        Console::Error->WriteLine(L"cudaMemcpy failed!");
        //        goto Error;
        //    }


        //Error:
        //    cudaFree(dev_c);
        //    cudaFree(dev_a);
        //    cudaFree(dev_b);

        //    return cudaStatus;
        //}
    };
}