#pragma once
#ifndef _CudaException_H_
#define _CudaException_H_

using namespace System;

namespace Corona
{
    public ref class CudaException : Exception
    {
    public:
        CudaException(String^ message) : Exception(message) {}
    };
}

#endif
