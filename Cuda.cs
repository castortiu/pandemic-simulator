﻿//using System;
//using Alea;

//namespace Corona
//{
//    public static unsafe class Cuda
//    {
//        [GpuParam]
//        private static deviceptr<int> counter;

//        static Cuda()
//        {
//            IntPtr counterPtr;
//            CUDAInterop.cuSafeCall(CUDAInterop.cuCtxSetCurrent(CurrentGpu.Context.Handle));
//            CUDAInterop.cuSafeCall(CUDAInterop.cuMemAlloc(&counterPtr, (IntPtr)2));
//            counter = new deviceptr<int>(counterPtr);
//        }

//        public static Gpu CurrentGpu => Gpu.Default;

//        public static GlobalArraySymbol<T> AllocateConstantArray<T>(T[] array)
//        {
//            GlobalArraySymbol<T> symbol = Gpu.DefineConstantArraySymbol<T>(array.Length);
//            CurrentGpu.Copy(array, symbol);
//            return symbol;
//        }

//        public static DeviceMemory<T> AllocateDevice<T>(int length, GlobalVariableSymbol<deviceptr<T>> kernelSymbolPtr, GlobalVariableSymbol<int> kernelSymbolLength)
//        {
//            DeviceMemory<T> memory = CurrentGpu.AllocateDevice<T>(length);
//            CurrentGpu.Copy(memory.Ptr, kernelSymbolPtr);
//            CurrentGpu.Copy(length, kernelSymbolLength);
//            return memory;
//        }

//        public static DeviceMemory<T> AllocateDevice<T>(T[] array, GlobalVariableSymbol<deviceptr<T>> kernelSymbolPtr, GlobalVariableSymbol<int> kernelSymbolLength)
//        {
//            DeviceMemory<T> memory = CurrentGpu.AllocateDevice(array);
//            CurrentGpu.Copy(memory.Ptr, kernelSymbolPtr);
//            if (kernelSymbolLength != null)
//            {
//                CurrentGpu.Copy(array.Length, kernelSymbolLength);
//            }
//            return memory;
//        }

//        public static DeviceMemory<T> AllocateDevice<T>(int width, int height, GlobalVariableSymbol<deviceptr<T>> kernelSymbolPtr, GlobalVariableSymbol<int> kernelSymbolSizeX, GlobalVariableSymbol<int> kernelSymbolSizeY)
//        {
//            DeviceMemory<T> memory = CurrentGpu.AllocateDevice<T>(width * height);
//            CurrentGpu.Copy(memory.Ptr, kernelSymbolPtr);
//            CurrentGpu.Copy(width, kernelSymbolSizeX);
//            CurrentGpu.Copy(height, kernelSymbolSizeY);
//            return memory;
//        }
        
//        //public static unsafe void GetAttribute()
//        //{
//        //    int test = 10;
//        //    int* pi = &test;
//        //    int id = CurrentGpu.Device.Id;
//        //    CUDAInterop.cudaError_enum tt = CUDAInterop.cuDeviceGetAttribute(pi, (CUDAInterop.CUdevice_attribute_enum)3, id);
//        //}

//        //public static unsafe void AllocateVar()
//        //{
//        //LibDevice.__pop
//        //    //CUDAInterop.cuMemcpy()
//        //    //int* d_data, h_data1, h_data2;
//        //    //var rc = default(CUDAInterop.cudaError_enum);
//        //    //if ((rc = CUDAInterop.cuMemAllocManaged((IntPtr*)&h_data1, (IntPtr)2000000000, 2)) != CUDAInterop.cudaError_enum.CUDA_SUCCESS)
//        //    //    throw new InvalidOperationException($"'cudaHostAlloc' is failed. CUDA Error Code = { (int)rc }({ rc })");

//        //    //if ((rc = CUDAInterop.cuMemAllocManaged((IntPtr*)&h_data2, (IntPtr)2000000000, 2)) != CUDAInterop.cudaError_enum.CUDA_SUCCESS)
//        //    //    throw new InvalidOperationException($"'cudaHostAlloc' is failed. CUDA Error Code = { (int)rc }({ rc })");
//        //}
//    }
//}
