﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Corona
{
    public unsafe class World : IDisposable
    {
        private readonly RandomGenerator rnd = new RandomGenerator();

        private readonly object rendering = new object();

        private readonly IndexedColor indexedWhite = new IndexedColor(Colors.White);

        private int screenLocationX;

        private int screenLocationY;

        public World(Engine engine, int width, int height)
        {
            Engine = engine;
            Cuda = Engine.Cuda;
            Width = width;
            Height = height;
            Cuda.Free();
            GC.Collect();

            PathogenRules pathogenRules = new PathogenRules
            {
                ContagiousRisk = Engine.Rules.ContagiousRisk,
                IncubationPeriodHigh = Engine.Rules.IncubationPeriodHigh,
                IncubationPeriodLow = Engine.Rules.IncubationPeriodLow,
                ImmunizationPeriodLow = Engine.Rules.ImmunizationPeriodLow,
                ImmunizationPeriodHigh = Engine.Rules.ImmunizationPeriodHigh,
                DeathPeriodLow = Engine.Rules.DeathPeriodLow,
                DeathPeriodHigh = Engine.Rules.DeathPeriodHigh,
            };

            PathogenRulesByAge[] rulesByAge = Engine.Rules.PopulationRules.Select(ruleByAge => new PathogenRulesByAge
            {
                BottomAge = ruleByAge.BottomAge,
                TopAge = ruleByAge.TopAge,
                MortalityRate = ruleByAge.MortalityRate,
                RegularMobility = ruleByAge.DefaultMobility,
                SymptomaticRisk = ruleByAge.SymptomaticRisk
            }).ToArray();

            Cuda.AllocatePlayground(Width, Height, pathogenRules, rulesByAge);
            Cuda.AllocateScreen(0, 0);
            AllocateStates(engine.Rules);
            AllocatePeople();
            Cuda.AllocateMetrics();
            Cuda.TransferPeopleToDevice();
        }

        public Engine Engine { get; }

        public CudaSupport Cuda { get; }

        public int Width { get; }

        public int Height { get; }

        public bool RenderMarkers { get; set; }

        public Metrics Metrics => Cuda.GetMetrics();

        public PathogenRulesByAge[] RulesByAge => Cuda.GetRulesMetrics();

        public Person GetPatientZero()
        {
            return Cuda.GetPatientZero();
        }

        public void ComputeNextMove(bool freeze)
        {
            lock (rendering)
            {
                Cuda.ComputeNextMove(screenLocationX, screenLocationY, freeze);
            }
        }

        public void RenderPeople(int locationX, int locationY, float zoom, WriteableBitmap bmp)
        {
            if (bmp == null)
                return;

            screenLocationX = locationX;
            screenLocationY = locationY;

            if (Cuda.ScreenWidth != bmp.BackBufferStride || Cuda.ScreenHeight != (int)bmp.Height)
            {
                lock (rendering)
                {
                    Cuda.AllocateScreen(bmp.BackBufferStride, (int)bmp.Height);
                    ComputeNextMove(true);
                }
            }

            if (RenderMarkers)
            {
                Cuda.RenderMarkers(screenLocationX,
                    screenLocationY,
                    indexedWhite.PaletteIndex,
                    zoom);
            }

            using BitmapContext bitmapContext2 = bmp.GetBitmapContext();
            byte* targetPtr = (byte*)bitmapContext2.Pixels;

            Cuda.TransferScreenToHost(targetPtr);
        }

        public void AllocatePeople()
        {
            Rules rules = Engine.Rules;
            int currentCount = 0;
            int locationStart = 0;

            Person[] peopleArray = new Person[rules.Population];
            int[,] map = new int[Height, Width];

            Task[] tasks = new Task[rules.PopulationRules.Count];
            for (int range = 0; range < rules.PopulationRules.Count; range++)
            {
                PopulationRule rule = rules.PopulationRules[range];
                rule.TotalPopulation = range < rules.PopulationRules.Count ?
                    (int)(rules.Population * (rule.Population / 100.0)) :
                    rules.Population - currentCount;

                int start = locationStart;
                byte ruleIndex = (byte) range;
                tasks[range] = Tasks.Run(() =>
                {
                    for (int idx = 0; idx < rule.TotalPopulation; idx++)
                    {
                        peopleArray[start + idx] = CreatePerson(rule, ruleIndex, map);
                    }
                });

                locationStart += rule.TotalPopulation;
            }
            Task.WaitAll(tasks);

            int symptomaticIndex = rnd.Next(peopleArray.Length);
            peopleArray[symptomaticIndex].State = (byte) PersonState.Infected;
            peopleArray[symptomaticIndex].ColorIndex = rules.Infected.PaletteIndex;
            peopleArray[symptomaticIndex].InfectionHour = (int) PersonState.Infected;
            Cuda.AllocatePeople(peopleArray);
        }

        private Person CreatePerson(PopulationRule rule, byte ruleByAge, int[,] map)
        {
            Person person = new Person(rule.State, ruleByAge)
            {
                State = rule.State,
                ColorIndex = rule.Color.PaletteIndex,
                Mobility = rule.DefaultMobility / 100f,
                Rule = ruleByAge
            };

            while (true)
            {
                int x = rnd.Next(Width);
                int y = rnd.Next(Height);

                if (Interlocked.CompareExchange(ref map[y, x], 1, 0) == 0)
                {
                    person.PositionX = x;
                    person.PositionY = y;
                    break;
                }
            }

            return person;
        }

        public void AllocateStates(Rules rules)
        {
            byte[] statesColors = new byte[65];
            statesColors[(byte) PersonState.Healthy] = rules.Healthy.PaletteIndex;
            statesColors[(byte) PersonState.Infected] = rules.Infected.PaletteIndex;
            statesColors[(byte) PersonState.Asymptomatic] = rules.Asymptomatic.PaletteIndex;
            statesColors[(byte) PersonState.Symptomatic] = rules.Symptomatic.PaletteIndex;
            statesColors[(byte) PersonState.Immunized] = rules.Immunized.PaletteIndex;
            statesColors[(byte) PersonState.Isolated] = rules.Isolated.PaletteIndex;
            statesColors[(byte) PersonState.Hospitalized] = rules.Hospitalized.PaletteIndex;
            statesColors[(byte) PersonState.Death] = rules.Death.PaletteIndex;

            Cuda.AllocateStates(statesColors);
        }

        public void Dispose()
        {
        }
    }
}
