﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Media;
using Corona.Annotations;
using Newtonsoft.Json;

namespace Corona
{
    public class Engine : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler UpdateLayout;
        public event EventHandler SimulationStarted;
        public event EventHandler SimulationCreated;
        public event EventHandler DayElapsed;
        private readonly StopwatchEx computeStopwatch;
        private readonly Timer timer;
        private string message;
        private bool isRunning;
        private Rules rules;
        private int width;
        private int height;
        private World world;

        private int timerPeriod = 40;
        private bool isSimulationCreated;
        private bool isReady;
        public const byte Healthy = 0;
        public const byte Infected = 1;
        public const byte Asymptomatic = 2;
        public const byte Symptomatic = 3;
        public const byte Isolated = 4;
        public const byte Hospitalized = 5;
        public const byte Death = 6;
        public const byte Immunized = 7;
        public const byte Gone = 8;

        public Engine()
        {
            timer = new Timer(Timer_Tick);
            Rules = new Rules();
            Rules.PopulationRules.Add(new PopulationRule(Rules)
            {
                BottomAge = 0,
                TopAge = 99,
                Population = 100,
                MobilityMeasure = 100,
                SymptomaticRisk = 50,
                DefaultMobility = 100,
                MortalityRate = 50,
                State = Healthy,
                Color = new IndexedColor(Colors.Green)
            });

            Width = Rules.WorldWidth;
            Height = Rules.WorldHeight;
            computeStopwatch = new StopwatchEx(10, () => OnPropertyChanged(nameof(ComputeTime)));
        }

        public CudaSupport Cuda { get; } = new CudaSupport();

        public World World => world;

        public double ComputeTime => computeStopwatch.Time;

        public ObservableCollection<PopulationRule> PopulationRules { get; } = new ObservableCollection<PopulationRule>();

        public int TimerPeriod
        {
            get => timerPeriod;
            set
            {
                timerPeriod = value;
                OnPropertyChanged(nameof(TimerPeriod));
            }
        }

        public int Width
        {
            get => width;
            set
            {
                width = value;
                OnPropertyChanged(nameof(Width));
            }
        }

        public int Height
        {
            get => height;
            set
            {
                height = value;
                OnPropertyChanged(nameof(Height));
            }
        }

        public Metrics Metrics { get; private set; }

        public bool IsRunning
        {
            get => isRunning;
            private set
            {
                isRunning = value;
                OnPropertyChanged(nameof(IsRunning));
            }
        }

        public bool IsSimulationCreated
        {
            get => isSimulationCreated;
            private set
            {
                isSimulationCreated = value;
                OnPropertyChanged(nameof(IsSimulationCreated));
            }
        }

        public bool IsReady
        {
            get => isReady;
            private set
            {
                isReady = value;
                OnPropertyChanged(nameof(IsReady));
            }
        }

        public string Message
        {
            get => message;
            private set
            {
                message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public Rules Rules
        {
            get => rules;
            set
            {
                IsReady = false;
                rules = value;
                Width = rules.WorldWidth;
                Height = rules.WorldHeight;
                PopulationRules.Clear();
                foreach (var populationRule in rules.PopulationRules)
                {
                    PopulationRules.Add(populationRule);
                }

                ResetStatistics();
                OnPropertyChanged(nameof(Rules));
                OnPropertyChanged(nameof(PopulationRules));
            }
        }

        public void ReduceSpeed()
        {
            if (TimerPeriod < 5)
                TimerPeriod += 1;
            else
                TimerPeriod = (int)(timerPeriod * 1.2);
        }

        public void RaiseSpeed()
        {
            TimerPeriod = (int)(timerPeriod * .8);
            if (timerPeriod < 0)
                TimerPeriod = 0;
        }

        private void Timer_Tick(object parameter)
        {
            StopTimer();

            computeStopwatch.Measure(() =>
            {
                world.ComputeNextMove(false);
            });

            RefreshCounters();

            if (TimerPeriod != 0)
            {
                UpdateLayout?.Invoke(this, EventArgs.Empty);
            }

            if (Metrics.CurrentHour % 24 == 0)
                DayElapsed?.Invoke(this, EventArgs.Empty);

            if (isRunning)
                StartTimer();

            if (Metrics.Infected == 0)
            {
                StopSimulation();
                DayElapsed?.Invoke(this, EventArgs.Empty);
            }
        }

        private void StartTimer()
        {
            try
            {
                timer.Change(timerPeriod, timerPeriod);
            }
            catch (ObjectDisposedException) { }
        }

        private void StopTimer()
        {
            try
            {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            catch (ObjectDisposedException) { }
        }

        public void CreateSimulation(Action onCreated)
        {
            Message = "Building environment...";
            Tasks.Run(() =>
            {
                world?.Dispose();
                world = new World(this, Width, Height);
                Message = string.Empty;
                ResetStatistics();

                IsSimulationCreated = true;
                IsReady = true;

                SimulationCreated?.Invoke(this, EventArgs.Empty);

                UpdateLayout?.Invoke(this, EventArgs.Empty);

                onCreated?.Invoke();
            });
        }

        public void ResetStatistics()
        {
            ResetCounters();
            RefreshCounters();
        }

        private void ResetCounters()
        {
            TimerPeriod = Rules.SimulationSpeed;
            foreach (var rulesPopulationRule in PopulationRules)
            {
                rulesPopulationRule.ResetCounters();
                rulesPopulationRule.RefreshCounter();
            }
        }

        private void RefreshCounters()
        {
            Metrics = World?.Metrics ?? new Metrics();
            OnPropertyChanged(nameof(Metrics));

            var rulesByAge = World?.RulesByAge;

            if (rulesByAge != null)
            {
                for (int idx = 0; idx < rulesByAge.Length; idx++)
                {
                    Rules.PopulationRules[idx].DeathPopulation = rulesByAge[idx].Death;
                    Rules.PopulationRules[idx].ImmunizedPopulation = rulesByAge[idx].Immunized;
                    Rules.PopulationRules[idx].RefreshCounter();
                }
            }
        }

        public void StartSimulation()
        {
            if (IsRunning)
            {
                StopTimer();
                IsRunning = false;
            }
            else
            {
                IsReady = false;
                IsRunning = true;
                SimulationStarted?.Invoke(this, EventArgs.Empty);
                StartTimer();
            }
        }

        public void StopSimulation()
        {
            IsRunning = false;
            IsReady = true;
            StopTimer();
        }

        public async void Load(string path)
        {
            using (StreamReader reader = File.OpenText(path))
            {
                string rule = await reader.ReadToEndAsync();
                Rules = JsonConvert.DeserializeObject<Rules>(rule);
                foreach (var populationRule in Rules.PopulationRules)
                {
                    populationRule.Rules = Rules;
                }
            }
            CreateSimulation(() => { });
        }

        public async void Save(string path)
        {
            if (path == null)
                return;

            Rules.Name = Path.GetFileNameWithoutExtension(path);

            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.Indented };

            string serialized = JsonConvert.SerializeObject(Rules, settings);

            using StreamWriter writer = File.CreateText(path);
            await writer.WriteLineAsync(serialized);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            timer?.Dispose();
            world?.Dispose();
        }
    }
}
