﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Corona.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Corona.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to años.
        /// </summary>
        public static string Age {
            get {
                return ResourceManager.GetString("Age", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edad entre:.
        /// </summary>
        public static string AgeBetween {
            get {
                return ResourceManager.GetString("AgeBetween", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to y.
        /// </summary>
        public static string And {
            get {
                return ResourceManager.GetString("And", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pandemia simulator.
        /// </summary>
        public static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asintomaticos.
        /// </summary>
        public static string AsymptomaticText {
            get {
                return ResourceManager.GetString("AsymptomaticText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Camas.
        /// </summary>
        public static string Beds {
            get {
                return ResourceManager.GetString("Beds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crear Entorno.
        /// </summary>
        public static string BuildEnvironment {
            get {
                return ResourceManager.GetString("BuildEnvironment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancelar.
        /// </summary>
        public static string CancelButton {
            get {
                return ResourceManager.GetString("CancelButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Graficas.
        /// </summary>
        public static string Charts {
            get {
                return ResourceManager.GetString("Charts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grafico de Progreso.
        /// </summary>
        public static string ChartTitle {
            get {
                return ResourceManager.GetString("ChartTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configuracion.
        /// </summary>
        public static string Configuration {
            get {
                return ResourceManager.GetString("Configuration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mortalidad Actual:.
        /// </summary>
        public static string CurrentMortality {
            get {
                return ResourceManager.GetString("CurrentMortality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ambiente oscuro.
        /// </summary>
        public static string DarkTheme {
            get {
                return ResourceManager.GetString("DarkTheme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiempo de fallecido:.
        /// </summary>
        public static string DeathInfectionTime {
            get {
                return ResourceManager.GetString("DeathInfectionTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fallecidos.
        /// </summary>
        public static string DeathText {
            get {
                return ResourceManager.GetString("DeathText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Descripcion.
        /// </summary>
        public static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saludables.
        /// </summary>
        public static string HealthyText {
            get {
                return ResourceManager.GetString("HealthyText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tamaño Horizontal:.
        /// </summary>
        public static string HorizontalSizeText {
            get {
                return ResourceManager.GetString("HorizontalSizeText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hospitalizados.
        /// </summary>
        public static string HospitalizedText {
            get {
                return ResourceManager.GetString("HospitalizedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiempo de inmunizacion:.
        /// </summary>
        public static string ImmunizationPeriod {
            get {
                return ResourceManager.GetString("ImmunizationPeriod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inmunizados.
        /// </summary>
        public static string ImmunizedText {
            get {
                return ResourceManager.GetString("ImmunizedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiempo de incubacion:.
        /// </summary>
        public static string IncubationPeriod {
            get {
                return ResourceManager.GetString("IncubationPeriod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Infectados.
        /// </summary>
        public static string InfectedText {
            get {
                return ResourceManager.GetString("InfectedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Riesgo de contagio:.
        /// </summary>
        public static string InfectionRisk {
            get {
                return ResourceManager.GetString("InfectionRisk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aislados.
        /// </summary>
        public static string IsolatedText {
            get {
                return ResourceManager.GetString("IsolatedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cargar Patogeno.
        /// </summary>
        public static string LoadVirusText {
            get {
                return ResourceManager.GetString("LoadVirusText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Movilidad.
        /// </summary>
        public static string Mobility {
            get {
                return ResourceManager.GetString("Mobility", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Movilidad durante pandemia:.
        /// </summary>
        public static string MobilityWhilePandemic {
            get {
                return ResourceManager.GetString("MobilityWhilePandemic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mortalidad.
        /// </summary>
        public static string Mortality {
            get {
                return ResourceManager.GetString("Mortality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crear Regla.
        /// </summary>
        public static string NewRule {
            get {
                return ResourceManager.GetString("NewRule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aceptar.
        /// </summary>
        public static string OKButton {
            get {
                return ResourceManager.GetString("OKButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Paciente Zero.
        /// </summary>
        public static string PatientZero {
            get {
                return ResourceManager.GetString("PatientZero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Poblacion:.
        /// </summary>
        public static string Population {
            get {
                return ResourceManager.GetString("Population", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Poblacion Actual:.
        /// </summary>
        public static string PopulationCurrent {
            get {
                return ResourceManager.GetString("PopulationCurrent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Population:.
        /// </summary>
        public static string PopulationText {
            get {
                return ResourceManager.GetString("PopulationText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Poblacion Total:.
        /// </summary>
        public static string PopulationTotal {
            get {
                return ResourceManager.GetString("PopulationTotal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rango:.
        /// </summary>
        public static string Range {
            get {
                return ResourceManager.GetString("Range", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Movilidad Normal:.
        /// </summary>
        public static string RegularMobility {
            get {
                return ResourceManager.GetString("RegularMobility", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Respiradores.
        /// </summary>
        public static string Respiradores {
            get {
                return ResourceManager.GetString("Respiradores", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rules &gt;&gt;.
        /// </summary>
        public static string RulesText {
            get {
                return ResourceManager.GetString("RulesText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rules.
        /// </summary>
        public static string RulesTitle {
            get {
                return ResourceManager.GetString("RulesTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grabar Patogeno.
        /// </summary>
        public static string SaveVirusText {
            get {
                return ResourceManager.GetString("SaveVirusText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2x Scala.
        /// </summary>
        public static string ScaleDouble {
            get {
                return ResourceManager.GetString("ScaleDouble", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1/2x Scala.
        /// </summary>
        public static string ScaleHalf {
            get {
                return ResourceManager.GetString("ScaleHalf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mostrar Guias.
        /// </summary>
        public static string ShowMarkers {
            get {
                return ResourceManager.GetString("ShowMarkers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Velocidad(-).
        /// </summary>
        public static string SpeedDown {
            get {
                return ResourceManager.GetString("SpeedDown", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Velocidad:.
        /// </summary>
        public static string SpeedText {
            get {
                return ResourceManager.GetString("SpeedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Velocidad(+).
        /// </summary>
        public static string SpeedUp {
            get {
                return ResourceManager.GetString("SpeedUp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Series Apiladas.
        /// </summary>
        public static string StackSeries {
            get {
                return ResourceManager.GetString("StackSeries", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comenzar Simulacion....
        /// </summary>
        public static string StartSimulation {
            get {
                return ResourceManager.GetString("StartSimulation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parar Simulacion....
        /// </summary>
        public static string StopSimulation {
            get {
                return ResourceManager.GetString("StopSimulation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Riesgo de ser sintomatico:.
        /// </summary>
        public static string SymptomaticRisk {
            get {
                return ResourceManager.GetString("SymptomaticRisk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sintomaticos.
        /// </summary>
        public static string SymptomaticText {
            get {
                return ResourceManager.GetString("SymptomaticText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tamaño Vertical:.
        /// </summary>
        public static string VerticalSizeText {
            get {
                return ResourceManager.GetString("VerticalSizeText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tipo de virus.
        /// </summary>
        public static string VirusType {
            get {
                return ResourceManager.GetString("VirusType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tipo de virus (*.vrs) | *.vrs.
        /// </summary>
        public static string VirusTypeWithExtension {
            get {
                return ResourceManager.GetString("VirusTypeWithExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Magnificacion:.
        /// </summary>
        public static string ZoomText {
            get {
                return ResourceManager.GetString("ZoomText", resourceCulture);
            }
        }
    }
}
