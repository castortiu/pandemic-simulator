﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Corona.Annotations;
using Corona.Tools;
using Image = System.Windows.Controls.Image;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace Corona
{
    public class ImageCanvas : Canvas, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private BackBuffer backBuffer;

        private Engine engine;
        private WriteableBitmap renderBitmap;
        private readonly TaskEx updateLayoutTask = new TaskEx();
        private readonly DispatcherEx dispatcherEx = new DispatcherEx();
        private readonly ReaderWriterLockSlim bmpLock = new ReaderWriterLockSlim();
        private readonly PixelFormat pixelFormat = PixelFormat.Format8bppIndexed;
        private GridEx playgroundGrid;
        private readonly StopwatchEx layoutStopwatch;
        private readonly StopwatchEx renderStopwatch;
        private double zoom = 1;
        private CustomScrollViewer playgroundScrollViewer;
        private readonly IndexedColor indexedBlack = new IndexedColor(Colors.Black);
        private IndexedColor backgroundColor;

        public ImageCanvas()
        {
            backgroundColor = indexedBlack;
            dispatcherEx.Dispatcher = Application.Current.Dispatcher;
            layoutStopwatch = new StopwatchEx(10, () => OnPropertyChanged(nameof(LayoutTime)));
            renderStopwatch = new StopwatchEx(10, () => OnPropertyChanged(nameof(RenderTime)));
        }

        public CustomScrollViewer PlaygroundScrollViewer
        {
            get => playgroundScrollViewer;
            set
            {
                playgroundScrollViewer = value;
                playgroundScrollViewer.ScrollChanged += PlaygroundScrollViewer_ScrollChanged;
                playgroundScrollViewer.SizeChanged += PlaygroundScrollViewer_SizeChanged;
            }
        }

        public IndexedColor BackgroundColor
        {
            get => backgroundColor;
            set
            {
                backgroundColor = value;
                RecreateViewPort();
            }
        }

        public GridEx PlaygroundGrid
        {
            get => playgroundGrid;
            set
            {
                playgroundGrid = value;
                playgroundGrid.ZoomChanged += PlaygroundGrid_ZoomChanged;
            }
        }

        private void PlaygroundScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetSize();
        }

        private void PlaygroundGrid_ZoomChanged(object sender, ZoomEventArgs e)
        {
            SetSize();
        }

        private void SetSize()
        {
            var parentWidth = playgroundScrollViewer.ActualWidth - SystemParameters.HorizontalScrollBarHeight;
            var parentHeight = playgroundScrollViewer.ActualHeight - SystemParameters.VerticalScrollBarWidth;

            zoom = playgroundGrid.Zoom;

            Width = Math.Ceiling(parentWidth / zoom);
            Height = Math.Ceiling(parentHeight / zoom);

            LayoutTransform = new ScaleTransform(zoom, zoom);

            RecreateViewPort();
        }

        private void PlaygroundScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Margin = new Thickness(PlaygroundScrollViewer.HorizontalOffset, PlaygroundScrollViewer.VerticalOffset, 0, 0);

            Engine?.World?.ComputeNextMove(true);
            UpdateLayoutMap();
        }

        public double LayoutTime => layoutStopwatch.Time;

        public double RenderTime => renderStopwatch.Time;

        public Engine Engine
        {
            get => engine;
            set
            {
                engine = value;
                engine.UpdateLayout += (s, e) => updateLayoutTask.RunWhenNotRunning(UpdateLayoutMap);
                engine.SimulationStarted += Engine_SimulationStarted;
                engine.SimulationCreated += Engine_SimulationCreated;
            }
        }

        private void Engine_SimulationStarted(object sender, EventArgs e)
        {
        }

        private void Engine_SimulationCreated(object sender, EventArgs e)
        {
            dispatcherEx.Invoke(() =>
            {
                backBuffer = new BackBuffer(engine.Width, engine.Height, pixelFormat);
                backBuffer.Clear(backgroundColor.PaletteIndex);
                PlaygroundScrollViewer.ScrollToHome();
            });
        }

        private void RecreateViewPort()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            lock (this)
            {
                Children.Clear();
                renderBitmap = new WriteableBitmap((int)Width, (int)Height, 96.0, 96.0, PixelFormats.Indexed8, IndexedColor.Palette);
                using (renderBitmap.GetBitmapContext())
                    Children.Add(new Image { Source = renderBitmap });
            }

            UpdateLayoutMap();
        }

        private void UpdateLayoutMap()
        {
            Invalidate();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (backBuffer == null)
                return;

            renderStopwatch.Measure(() =>
                bmpLock.OnWriterLock(() =>
                {
                    Engine.World.RenderPeople(
                        (int)(PlaygroundScrollViewer.HorizontalOffset / zoom), 
                        (int)(PlaygroundScrollViewer.VerticalOffset / zoom),
                        (float)zoom,
                        renderBitmap);
                }));
        }

        public void Invalidate()
        {
            dispatcherEx.BeginInvokeWhenNotRunning(DispatcherPriority.Send, InvalidateVisual);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
