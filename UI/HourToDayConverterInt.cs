﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Corona
{
    [ValueConversion(typeof(int), typeof(int))]
    public class HourToDayConverterInt : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,  CultureInfo culture)
        {
            if (value == null)
                return false;

            return (int) value / 24;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;

            return int.Parse((string) value) * 24;
        }
    }
}
