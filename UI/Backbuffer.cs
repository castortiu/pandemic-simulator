﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace Corona
{
    public class BackBuffer
    {
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern unsafe byte* memcpy(byte* dst, byte* src, int count);

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern unsafe void memset(byte* dst, int filler, int count);

        private int bytesCount;
        private byte[] buffer;
        private int stride;

        public BackBuffer(int width, int height, PixelFormat pixelFormat)
        {
            Width = width;
            Height = height;
            SetBytesCount(pixelFormat);
            stride = width * bytesCount + width % 4;
            buffer = new byte[stride * Height];
        }

        public BackBuffer(int width, int height, System.Windows.Media.PixelFormat pixelFormat)
        {
            Width = width;
            Height = height;
            SetBytesCount(pixelFormat);
            stride = width * bytesCount + width % 4;
            buffer = new byte[stride * Height];
        }

        public PixelFormat PixelFormat { get; private set; }

        public int Width { get; }

        public int Height { get; }

        public unsafe void Clear(byte paletteIndex)
        {
            fixed (byte* sourcePtr = &buffer[0])
            {
                memset(sourcePtr, paletteIndex, buffer.Length);
            }
        }

        public unsafe void Clear(int left, int top, int width, int height, byte paletteIndex)
        {
            int start = left * bytesCount + top * stride;
            if (start > Width * Height)
                return;

            fixed (byte* sourcePtr = &buffer[start])
            {
                byte* targetPtr = sourcePtr;
                for (int idx = 0; idx < Math.Min(height, Height - top); idx++)
                {
                    memset(targetPtr, paletteIndex, Math.Max(0, Math.Min((Width - left) * bytesCount, width * bytesCount)));
                    targetPtr += stride;
                }
            }
        }

        //public void Clear(Rectangle bounds, IndexedColor color)
        //{
        //    Clear(bounds, color.PaletteIndex);
        //}

        //public unsafe void Clear(Rectangle bounds, byte paletteIndex)
        //{
        //    fixed (byte* sourcePtr = &buffer[bounds.X * bytesCount + bounds.Y * stride])
        //    {
        //        byte* targetPtr = sourcePtr;
        //        for (int idx = 0; idx < bounds.Height; idx++)
        //        {
        //            memset(targetPtr, paletteIndex, bounds.Width * bytesCount);
        //            targetPtr += stride;
        //        }
        //    }
        //}

        public static unsafe void Clear(WriteableBitmap bmp, IndexedColor color)
        {
            byte paletteIndex = color.PaletteIndex;
            using (BitmapContext bitmapContext2 = bmp.GetBitmapContext())
            {
                byte* targetPtr = (byte*) bitmapContext2.Pixels;
                memset(targetPtr, paletteIndex, (int) (bmp.BackBufferStride * bmp.Height));
            }
        }

        //public void SetPixel(System.Windows.Point position, Color color)
        //{
        //    byte paletteIndex = ColorEx.GetColorIndexFromColor(color);
        //    SetPixel(position, paletteIndex);
        //}

        public void SetPixelFast(int x, int y, byte paletteIndex)
        {
            buffer[x + y * stride] = paletteIndex;
        }

        public void SetPixel(int x, int y, byte paletteIndex)
        {
            int location = x + y * stride;
            if (location >= 0 && location < buffer.Length)
                buffer[location] = paletteIndex;
        }

        public unsafe void CopyToBitmap(WriteableBitmap bmp)
        {
            if (bmp == null)
                return;

            if ((int)bmp.Width != Width ||
                (int)bmp.Height != Height ||
                bmp.Format != ToMediaPixelFormat())
                throw new ArgumentException("Images cannot be different");

            fixed (byte* sourcePtr = &buffer[0])
            {
                using (BitmapContext bitmapContext2 = bmp.GetBitmapContext())
                {
                    //byte* targetPtr = (byte*)bitmapContext2.Pixels;
                    //memcpy(targetPtr, sourcePtr, Stride * Height);
                }
            }
        }

        public unsafe void CopyToBitmap(Point location, WriteableBitmap bmp)
        {
            if (bmp == null || location.X * bytesCount + location.Y * stride > buffer.Length)
                return;

            fixed (byte* sourcePtr0 = &buffer[location.X * bytesCount + location.Y * stride])
            {
                byte* sourcePtr = sourcePtr0;
                using (BitmapContext bitmapContext2 = bmp.GetBitmapContext())
                {
                    byte* targetPtr = (byte*)bitmapContext2.Pixels;
                    for (int idx = 0; idx < Math.Min(this.Height - location.Y, bmp.Height); idx++)
                    {
                        memcpy(targetPtr, sourcePtr, Math.Max(0, Math.Min(Width - location.X, bmp.BackBufferStride)));
                        
                        sourcePtr += stride;
                        targetPtr += bmp.BackBufferStride;
                    }
                }
            }
        }

        public unsafe void CopyToBitmap(Rectangle bounds, WriteableBitmap bmp)
        {
            if (bmp == null)
                return;

            ////if ((int)bmp.Width != Width ||
            ////    (int)bmp.Height != Height ||
            ////    bmp.Format != ToMediaPixelFormat())
            ////    throw new ArgumentException("Images cannot be different");
            fixed (byte* sourcePtr0 = &buffer[bounds.X * bytesCount + bounds.Y * stride])
            {
                byte* sourcePtr = sourcePtr0;
                using (BitmapContext bitmapContext2 = bmp.GetBitmapContext())
                {
                    byte* targetPtr = (byte*)bitmapContext2.Pixels + bounds.X * bytesCount + bounds.Y * bmp.BackBufferStride;
                    for (int idx = 0; idx < bounds.Height; idx++)
                    {
                        memcpy(targetPtr, sourcePtr, bounds.Width * bytesCount);

                        sourcePtr += stride;
                        targetPtr += bmp.BackBufferStride;
                    }
                }
            }
        }

        public static System.Windows.Media.PixelFormat ToMediaPixelFormat(PixelFormat pixelFormat)
        {
            if (pixelFormat == PixelFormat.Format8bppIndexed)
                return PixelFormats.Indexed8;
            if (pixelFormat == PixelFormat.Format24bppRgb)
                return PixelFormats.Bgr24;
            if (pixelFormat == PixelFormat.Format32bppRgb)
                return PixelFormats.Bgr32;
            if (pixelFormat == PixelFormat.Format32bppArgb)
                return PixelFormats.Bgra32;
            if (pixelFormat == PixelFormat.Format32bppPArgb)
                return PixelFormats.Pbgra32;

            throw new ArgumentException("Invalid Format");
        }

        public System.Windows.Media.PixelFormat ToMediaPixelFormat()
        {
            return ToMediaPixelFormat(PixelFormat);
        }

        private void SetBytesCount(PixelFormat pixelFormat)
        {
            PixelFormat = pixelFormat;

            if (pixelFormat == PixelFormat.Format8bppIndexed)
                bytesCount = 1;
            else if (pixelFormat == PixelFormat.Format24bppRgb)
                bytesCount = 3;
            else if (pixelFormat == PixelFormat.Format32bppArgb ||
                    pixelFormat == PixelFormat.Format32bppPArgb ||
                    pixelFormat == PixelFormat.Format32bppRgb)
                bytesCount = 4;
            else
                throw new ArgumentException("Invalid Pixel Format");
        }

        private void SetBytesCount(System.Windows.Media.PixelFormat pixelFormat)
        {
            if (pixelFormat == PixelFormats.Indexed8)
            {
                PixelFormat = PixelFormat.Format8bppIndexed;
                bytesCount = 1;
            }
            else if (pixelFormat == PixelFormats.Rgb24)
            {
                PixelFormat = PixelFormat.Format24bppRgb;
                bytesCount = 3;
            }
            else if (pixelFormat == PixelFormats.Bgr32)
            {
                PixelFormat = PixelFormat.Format32bppRgb;
                bytesCount = 4;
            }
            else if (pixelFormat == PixelFormats.Bgra32)
            {
                PixelFormat = PixelFormat.Format32bppArgb;
                bytesCount = 4;
            }
            else if (pixelFormat == PixelFormats.Pbgra32)
            {
                PixelFormat = PixelFormat.Format32bppPArgb;
                bytesCount = 4;
            }
            else
                throw new ArgumentException("Invalid Pixel Format");
        }
    }
}
