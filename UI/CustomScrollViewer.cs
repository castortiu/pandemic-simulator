﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Corona
{
    public class CustomScrollViewer : ScrollViewer
    {
        private Point scrollMousePoint;
        private double horizontalOffset;
        private double verticalOffset;

        protected override void OnManipulationDelta(ManipulationDeltaEventArgs e)
        {
            base.OnManipulationDelta(e);
            e.ReportBoundaryFeedback(new ManipulationDelta(new Vector(0, 0), 0, new Vector(0, 0), new Vector(0, 0)));
        }

        public Engine Engine { get; set; }

        public ImageCanvas PlaygroundCanvas { get; set; }

        public GridEx PlaygroundGrid { get; set; }

        protected override void OnScrollChanged(ScrollChangedEventArgs e)
        {
            if (Engine != null && Engine.IsSimulationCreated && Engine.IsRunning == false)
                PlaygroundCanvas.Invalidate();

            base.OnScrollChanged(e);
        }

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            scrollMousePoint = e.GetPosition(this);

            if (scrollMousePoint.X >= ActualWidth - SystemParameters.HorizontalScrollBarHeight ||
                scrollMousePoint.Y >= ActualHeight - SystemParameters.VerticalScrollBarWidth)
            {
                base.OnPreviewMouseLeftButtonDown(e);
                return;
            }

            horizontalOffset = HorizontalOffset;
            verticalOffset = VerticalOffset;
            CaptureMouse();
            e.Handled = true;

            base.OnPreviewMouseLeftButtonDown(e);
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (IsMouseCaptured)
            {
                Point position = e.GetPosition(this);
                ScrollToHorizontalOffset(horizontalOffset + (scrollMousePoint.X - position.X));
                ScrollToVerticalOffset(verticalOffset + (scrollMousePoint.Y - position.Y));
            }

            base.OnPreviewMouseMove(e);
        }

        protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();

            base.OnPreviewMouseUp(e);
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            Point center = e.GetPosition(this);

            PlaygroundGrid.SetZoom(PlaygroundGrid.Zoom * (Math.Sign(e.Delta) > 0 ? 1.2 : .83333333333333333333333333333333333), center);

            e.Handled = true;

            base.OnPreviewMouseWheel(e);
        }
    }
}
