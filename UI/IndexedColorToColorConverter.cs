﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Corona
{
    [ValueConversion(typeof(IndexedColor), typeof(Color))]
    public class IndexedColorToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;

            return ((IndexedColor) value).Color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            return new IndexedColor((Color)value);
        }
    }
}
