﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Corona
{
    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,  CultureInfo culture)
        {
            if (value == null)
                return false;

            if (targetType != typeof(string))
                throw new InvalidOperationException("The target must be a double");

            return ((double)value).ToString("0.00");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
