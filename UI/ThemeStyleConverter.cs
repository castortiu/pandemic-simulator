﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Corona
{
    [ValueConversion(typeof(ResourceDictionary), typeof(Style))]
    public class ThemeStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,  CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            ResourceDictionary resourceDictionary = (ResourceDictionary) value;
            return resourceDictionary["DefaultTheme" + parameter];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
