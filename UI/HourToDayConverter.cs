﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Corona
{
    [ValueConversion(typeof(int), typeof(string))]
    public class HourToDayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,  CultureInfo culture)
        {
            if (value == null)
                return false;

            return "Dia:" + (uint)value / 24 + " Hora:" + ((uint)value % 24).ToString().PadLeft(2, '0');
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
