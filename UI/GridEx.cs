﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Corona.Annotations;

namespace Corona
{
    public delegate void ZoomEventHandler(object sender, ZoomEventArgs e);

    public class ZoomEventArgs : EventArgs
    {
        public ZoomEventArgs(double zoom)
        {
            Zoom = zoom;
        }

        public double Zoom { get; set; }
    }

    public class GridEx : Grid, INotifyPropertyChanged
    {
        public event ZoomEventHandler ZoomChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly object mutex = new object();
        private Engine engine;
        private readonly DispatcherEx dispatcherEx = new DispatcherEx();
        private int width = -1;
        private int height = -1;
        private double previousWidth;
        private double previousHeight;

        public GridEx()
        {
            dispatcherEx.Dispatcher = Application.Current.Dispatcher;
            Zoom = 1;
        }

        public ImageCanvas Canvas { get; set; }

        public Grid MasterGrid { get; set; }

        public CustomScrollViewer PlaygroundScrollViewer { get; set; }

        public double Zoom { get; private set; }

        public Engine Engine
        {
            get => engine;
            set
            {
                engine = value;
                engine.SimulationCreated += Engine_SimulationCreated;
            }
        }

        private void Engine_SimulationCreated(object sender, EventArgs e)
        {
            dispatcherEx.Invoke(() =>
            {
                width = engine.Width;
                height = engine.Height;
                SetZoom(1, new Point(0, 0));
            });
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            previousWidth = Width;
            previousHeight = Height;

            base.OnRenderSizeChanged(sizeInfo);
        }

        public void SetZoom(double scale, Point center)
        {
            if (width == -1 || height == -1)
                return;

            Zoom = scale;

            double desiredZoom = 0;
            if (width * Zoom < PlaygroundScrollViewer.ActualWidth)
                desiredZoom = Math.Max(desiredZoom, PlaygroundScrollViewer.ActualWidth / width);
            if (height * Zoom < PlaygroundScrollViewer.ActualHeight)
                desiredZoom = Math.Max(desiredZoom, PlaygroundScrollViewer.ActualWidth / width);

            if (desiredZoom.Equals(0) == false)
                Zoom = desiredZoom;

            Width = (int)(width * Zoom);
            Height = (int)(height * Zoom);

            MasterGrid.Width = Width;
            MasterGrid.Height = Height;

            ZoomChanged?.Invoke(this, new ZoomEventArgs(Zoom));

            if (Double.IsNaN(previousWidth) || Double.IsNaN(previousHeight))
                return;

            center = new Point(center.X + Canvas.Margin.Left, center.Y + Canvas.Margin.Top);

            double offsetX = Width * (center.X / Width) - Width * (center.X / previousWidth);
            double offsetY = Height * (center.Y / Height) - Height * (center.Y / previousHeight);

            PlaygroundScrollViewer.ScrollToHorizontalOffset(PlaygroundScrollViewer.HorizontalOffset - offsetX);
            PlaygroundScrollViewer.ScrollToVerticalOffset(PlaygroundScrollViewer.VerticalOffset - offsetY);

            previousWidth = Width;
            previousHeight = Height;
            OnPropertyChanged(nameof(Zoom));
        }

        protected override void OnTouchDown(TouchEventArgs e)
        {
            CaptureTouch(e.TouchDevice);

            base.OnTouchDown(e);
        }

        protected override void OnTouchUp(TouchEventArgs e)
        {
            ReleaseTouchCapture(e.TouchDevice);

            base.OnTouchUp(e);
        }

        protected override void OnManipulationStarting(ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = PlaygroundScrollViewer;
            e.Handled = true;

            base.OnManipulationStarting(e);
        }

        protected override void OnManipulationDelta(ManipulationDeltaEventArgs e)
        {
            if (e.Manipulators.Count() >= 2)
            {
                Point? centerOfPinch = (e.ManipulationContainer as FrameworkElement)?.TranslatePoint(e.ManipulationOrigin, PlaygroundScrollViewer);

                if (centerOfPinch == null)
                    return;

                ManipulationDelta deltaManipulation = e.DeltaManipulation;

                e.Handled = true;

                SetZoom(Zoom * deltaManipulation.Scale.X, centerOfPinch.Value);
            }
            else
            {
                PlaygroundScrollViewer.ScrollToHorizontalOffset(PlaygroundScrollViewer.HorizontalOffset - e.DeltaManipulation.Translation.X);
                PlaygroundScrollViewer.ScrollToVerticalOffset(PlaygroundScrollViewer.VerticalOffset - e.DeltaManipulation.Translation.Y);
            }

            base.OnManipulationDelta(e);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
