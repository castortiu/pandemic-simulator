﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Corona.Annotations;
using MessageBox = System.Windows.MessageBox;

namespace Corona
{
    /// <summary>
    /// Interaction logic for RulesWindow.xaml
    /// </summary>
    public partial class RulesWindow : INotifyPropertyChanged
    {
        private Rules rules;
        private PopulationRule rule;

        public RulesWindow()
        {
            RulesCollection = new ObservableCollection<PopulationRule>();
            InitializeComponent();
            this.DataContext = this;
        }

        public Rules Rules
        {
            get => rules;
            set
            {
                rules = value;
                rule = new PopulationRule(rules);
                RulesCollection = new ObservableCollection<PopulationRule>();
                foreach (var populationRule in rules.PopulationRules.OrderBy(n => n.BottomAge))
                {
                    RulesCollection.Add(populationRule);
                }
            }
        }

        public int WorldWidth
        {
            get => rules.WorldWidth;
            set
            {
                rules.WorldWidth = value;
                OnPropertyChanged(nameof(WorldWidth));
            }
        }

        public int WorldHeight
        {
            get => rules.WorldHeight;
            set
            {
                rules.WorldHeight = value;
                OnPropertyChanged(nameof(WorldHeight));
            }
        }

        public int SimulationSpeed
        {
            get => rules.SimulationSpeed;
            set
            {
                rules.SimulationSpeed = value;
                OnPropertyChanged(nameof(SimulationSpeed));
            }
        }

        public float ContagiousRisk
        {
            get => rules.ContagiousRisk;
            set
            {
                rules.ContagiousRisk = value;
                OnPropertyChanged(nameof(ContagiousRisk));
            }
        }

        public int IncubationPeriodLow
        {
            get => rules.IncubationPeriodLow;
            set
            {
                rules.IncubationPeriodLow = value;
                OnPropertyChanged(nameof(IncubationPeriodLow));
            }
        }

        public int IncubationPeriodHigh
        {
            get => rules.IncubationPeriodHigh;
            set
            {
                rules.IncubationPeriodHigh = value;
                OnPropertyChanged(nameof(IncubationPeriodHigh));
            }
        }

        public int ImmunizationPeriodLow
        {
            get => rules.ImmunizationPeriodLow;
            set
            {
                rules.ImmunizationPeriodLow = value;
                OnPropertyChanged(nameof(ImmunizationPeriodLow));
            }
        }

        public int ImmunizationPeriodHigh
        {
            get => rules.ImmunizationPeriodHigh;
            set
            {
                rules.ImmunizationPeriodHigh = value;
                OnPropertyChanged(nameof(ImmunizationPeriodHigh));
            }
        }

        public int DeathPeriodLow
        {
            get => rules.DeathPeriodLow;
            set
            {
                rules.DeathPeriodLow = value;
                OnPropertyChanged(nameof(DeathPeriodLow));
            }
        }

        public int Beds
        {
            get => rules.Beds;
            set
            {
                rules.Beds = value;
                OnPropertyChanged(nameof(Beds));
            }
        }

        public IndexedColor BedsColor
        {
            get => rules.BedsColor;
            set
            {
                rules.BedsColor = value;
                OnPropertyChanged(nameof(BedsColor));
            }
        }

        public int Respirators
        {
            get => rules.Respirators;
            set
            {
                rules.Respirators = value;
                OnPropertyChanged(nameof(Respirators));
            }
        }

        public IndexedColor RespiratorsColor
        {
            get => rules.RespiratorsColor;
            set
            {
                rules.RespiratorsColor = value;
                OnPropertyChanged(nameof(RespiratorsColor));
            }
        }

        public int DeathPeriodHigh
        {
            get => rules.DeathPeriodHigh;
            set
            {
                rules.DeathPeriodHigh = value;
                OnPropertyChanged(nameof(DeathPeriodHigh));
            }
        }
        public int TotalPopulation
        {
            get => rules.Population;
            set
            {
                rules.Population = value;
                OnPropertyChanged(nameof(TotalPopulation));
            }
        }

        public IndexedColor Infected
        {
            get => rules.Infected;
            set
            {
                rules.Infected = value;
                OnPropertyChanged(nameof(Infected));
            }
        }

        public IndexedColor Asymptomatic
        {
            get => rules.Asymptomatic;
            set
            {
                rules.Asymptomatic = value;
                OnPropertyChanged(nameof(Asymptomatic));
            }
        }

        public IndexedColor Symptomatic
        {
            get => rules.Symptomatic;
            set
            {
                rules.Symptomatic = value;
                OnPropertyChanged(nameof(Symptomatic));
            }
        }

        public IndexedColor Immunized
        {
            get => rules.Immunized;
            set
            {
                rules.Immunized = value;
                OnPropertyChanged(nameof(Immunized));
            }
        }

        public IndexedColor Isolated
        {
            get => rules.Isolated;
            set
            {
                rules.Isolated = value;
                OnPropertyChanged(nameof(Isolated));
            }
        }

        public IndexedColor Hospitalized
        {
            get => rules.Hospitalized;
            set
            {
                rules.Hospitalized = value;
                OnPropertyChanged(nameof(Hospitalized));
            }
        }

        public IndexedColor Death
        {
            get => rules.Death;
            set
            {
                rules.Death = value;
                OnPropertyChanged(nameof(Death));
            }
        }

        public byte BottomAge
        {
            get => rule.BottomAge;
            set
            {
                rule.BottomAge = value;
                OnPropertyChanged(nameof(BottomAge));
            }
        }

        public byte TopAge
        {
            get => rule.TopAge;
            set
            {
                rule.TopAge = value;
                OnPropertyChanged(nameof(TopAge));
            }
        }

        public byte Population
        {
            get => rule.Population;
            set
            {
                rule.Population = value;
                OnPropertyChanged(nameof(Population));
            }
        }

        public byte NormalMobility
        {
            get => rule.DefaultMobility;
            set
            {
                rule.DefaultMobility = value;
                OnPropertyChanged(nameof(NormalMobility));
            }
        }

        public float Mortality
        {
            get => rule.MortalityRate;
            set
            {
                rule.MortalityRate = value;
                OnPropertyChanged(nameof(Mortality));
            }
        }

        public byte ReduceMobility
        {
            get => rule.MobilityMeasure;
            set
            {
                rule.MobilityMeasure = value;
                OnPropertyChanged(nameof(ReduceMobility));
            }
        }

        public float SymptomaticRisk
        {
            get => rule.SymptomaticRisk;
            set
            {
                rule.SymptomaticRisk = value;
                OnPropertyChanged(nameof(SymptomaticRisk));
            }
        }

        public IndexedColor Color
        {
            get => rule.Color;
            set
            {
                rule.Color = value;
                OnPropertyChanged(nameof(Color));
            }
        }

        public ObservableCollection<PopulationRule> RulesCollection { get; private set; }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DoubleValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            if (TotalPopulation < 0)
            {
                MessageBox.Show("Poblacion tiene que ser mayor a cero", "Numero de poblacion invalida", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            double percentage = RulesCollection.Sum(n => n.Population);
            if (percentage.Equals(100.0) == false)
            {
                MessageBox.Show("La suma del porcentage de las reglas deben sumar 100%", "Numero de poblacion invalida", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            rules.PopulationRules.Clear();
            foreach (var populationRule in RulesCollection)
            {
                rules.PopulationRules.Add(populationRule);
            }
            this.DialogResult = true;
            this.Close();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            RulesCollection.Add(rule.Clone());
            rule = new PopulationRule(Rules) { Color = new IndexedColor(Colors.Black) };
            OnPropertyChanged(nameof(BottomAge));
            OnPropertyChanged(nameof(TopAge));
            OnPropertyChanged(nameof(Population));
            OnPropertyChanged(nameof(NormalMobility));
            OnPropertyChanged(nameof(Mortality));
            OnPropertyChanged(nameof(ReduceMobility));
            OnPropertyChanged(nameof(Color));
        }

        private void ButtonRemover_Click(object sender, RoutedEventArgs e)
        {
            if (RulesList.SelectedIndex == -1)
                return;

            RulesCollection.RemoveAt(RulesList.SelectedIndex);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void RulesList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(((ListBox)sender).SelectedItem is PopulationRule populationRule))
                return;

            rule = populationRule;
            BottomAge = rule.BottomAge;
            TopAge = rule.TopAge;
            Population = rule.Population;
            NormalMobility = rule.DefaultMobility;
            Mortality = rule.MortalityRate;
            ReduceMobility = rule.MobilityMeasure;
            Color = rule.Color;
            SymptomaticRisk = rule.SymptomaticRisk;
        }
    }
}
