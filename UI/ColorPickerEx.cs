﻿using Xceed.Wpf.Toolkit;

namespace Corona
{
    public class ColorPickerEx : ColorPicker
    {
        public ColorPickerEx()
        {
            AvailableColors.Clear();
            foreach (var webPaletteColor in IndexedColor.Palette.Colors)
            {
                AvailableColors.Add(new ColorItem(webPaletteColor, webPaletteColor.ToString()));
            }
            ShowStandardColors = false;
            ShowTabHeaders = false;
            AvailableColorsSortingMode = ColorSortingMode.Alphabetical;
            MaxWidth = 360;
            MaxDropDownWidth = 360;
        }
    }
}
