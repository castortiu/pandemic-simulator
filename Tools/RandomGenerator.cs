﻿using System;
using System.Threading;

namespace Corona
{
    public class RandomGenerator
    {
        private readonly Random[] rndGenerator = new Random[1000];

        private const int MaxValue = 10000;

        public RandomGenerator()
        {
            Random masterRnd = new Random();
            for (int idx = 0; idx < rndGenerator.Length; idx++)
                rndGenerator[idx] = new Random(masterRnd.Next(MaxValue) * idx);
        }

        public int Next(int maxValue)
        {
            return rndGenerator[Thread.CurrentThread.ManagedThreadId].Next(maxValue);
        }

        public double NextDouble(int maxValue)
        {
            return rndGenerator[Thread.CurrentThread.ManagedThreadId].NextDouble() * maxValue;
        }

        public void ChangeSeed()
        {
            int seed = Next(MaxValue);
            rndGenerator[Thread.CurrentThread.ManagedThreadId] = new Random(seed);
        }
    }
}
