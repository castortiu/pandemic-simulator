﻿using System;
using System.Threading;
using System.Windows.Threading;

namespace Corona
{
    public class DispatcherEx
    {
        private DispatcherOperation dispatcherOperation;

        public Dispatcher Dispatcher { get; set; }

        public void Invoke(Action action)
        {
            Dispatcher.Invoke(action, DispatcherPriority.Send);
        }

        public void BeginInvokeWhenNotRunning(DispatcherPriority priority, Action action)
        {
            if (dispatcherOperation == null ||
                dispatcherOperation.Status == DispatcherOperationStatus.Completed ||
                dispatcherOperation.Status == DispatcherOperationStatus.Aborted)
            {
                dispatcherOperation = Dispatcher.BeginInvoke(DispatcherPriority.Send, action);
            }
        }
    }
}
