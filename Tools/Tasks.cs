﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

namespace Corona
{
    public static class Tasks
    {
        private static readonly int processorCount;

        static Tasks()
        {
            processorCount = Environment.ProcessorCount;
        }

        public static Task Run<T>(Action<T> action, T parameter)
        {
            return Run(() => action(parameter));
        }

        public static Task Run<T, T1>(Action<T, T1> action, T parameter1, T1 parameter2)
        {
            return Run(() => action(parameter1, parameter2));
        }

        public static void RunAsParallel(int count, Action<int, int> action)
        {
            var tasks = new Task[processorCount];
            int idx = 0;
            int split = count / processorCount;
            for (; idx < processorCount - 1; idx++)
                tasks[idx] = Run(action, idx * split, idx * split + split);
            tasks[idx] = Run(action, idx * split, count);

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ex)
            {
                Debug.WriteLine("Exception Running Tasks:" + ex.Message);
            }
        }

        public static Task Run(Action action)
        {
            return Task.Run(() =>
            {
                try
                {
                    action?.Invoke();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Debug.WriteLine(e);
                    throw;
                }
            });
        }
    }
}
