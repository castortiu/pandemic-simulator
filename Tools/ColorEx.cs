﻿using System;
using System.Collections.Concurrent;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;

namespace Corona
{
    public class IndexedColor
    {
        private Color color;

        public IndexedColor(Color color)
        {
            Color = color;
        }

        public Color Color 
        { 
            get => color;
            set
            {
                PaletteIndex = GetColorIndexFromColor(value);
                color = GetColorFromIndex(PaletteIndex);
            }
        }

        [JsonIgnore]
        public byte PaletteIndex { get; set; }


        public static readonly BitmapPalette Palette = BitmapPalettes.Halftone256;

        private static readonly ConcurrentDictionary<Color, byte> nearestPaletteColors = new ConcurrentDictionary<Color, byte>();

        public static Color GetPaletteColor(Color color)
        {
            return GetColorFromIndex(GetColorIndexFromColor(color));
        }

        public static Color GetColorFromIndex(byte index)
        {
            return Palette.Colors[index];
        }

        private static byte GetColorIndexFromColor(Color color)
        {
            return nearestPaletteColors.GetOrAdd(color, c =>
            {
                byte paletteIndex = 0;
                double distance = double.MaxValue;
                for (int idx = 0; idx < Palette.Colors.Count; idx++)
                {
                    Color o = Palette.Colors[idx];

                    var r = Math.Pow(Convert.ToDouble(o.R) - c.R, 2.0);
                    var g = Math.Pow(Convert.ToDouble(o.G) - c.G, 2.0);
                    var b = Math.Pow(Convert.ToDouble(o.B) - c.B, 2.0);

                    var temp = Math.Sqrt(b + g + r);

                    // explore the result and store the nearest color
                    if (temp.Equals(0.0))
                    {
                        paletteIndex = (byte)idx;
                        break;
                    }

                    if (temp < distance)
                    {
                        distance = temp;
                        paletteIndex = (byte)idx;
                    }
                }

                return paletteIndex;
            });
        }
    }
}
