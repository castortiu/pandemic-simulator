﻿using System;
using System.Threading;
using System.Windows.Media;

namespace Corona
{
    public static class Extensions
    {
        //private const int MinLightness = 1;
        //private const int MaxLightness = 10;
        //private const float MinLightnessCoficient = 1f;
        //private const float MaxLightnessCoficient = 0.4f;

        public static void Fill<T>(this T[] originalArray, T with)
        {
            for (int i = 0; i < originalArray.Length; i++)
            {
                originalArray[i] = with;
            }
        }

        public static Color ChangeLightness(this Color color, double factor)
        {
            return Color.FromArgb(color.A, (byte)(color.R * factor), (byte)(color.G * factor), (byte)(color.B * factor));
        }

        public static Color ChangeOpacity(this Color color, double factor)
        {
            return Color.FromArgb((byte)(color.A * factor), color.R, color.G, color.B);
        }

        //public static Color ChangeLightness(this Color color, int lightness)
        //{
        //    if (lightness < MinLightness)
        //        lightness = MinLightness;
        //    else if (lightness > MaxLightness)
        //        lightness = MaxLightness;

        //    float coficient = MinLightnessCoficient +
        //                 (
        //                     (lightness - MinLightness) *
        //                     ((MaxLightnessCoficient - MinLightnessCoficient) / (MaxLightness - MinLightness))
        //                 );

        //    return Color.FromArgb(color.A, (byte)(color.R * coficient), (byte)(color.G * coficient),
        //        (byte)(color.B * coficient));
        //}

        public static void OnWriterLock(this ReaderWriterLockSlim lockSlim, Action action)
        {
            lockSlim.EnterWriteLock();
            try
            {
                action();
            }
            finally
            {
                lockSlim.ExitWriteLock();
            }
        }
    }
}
