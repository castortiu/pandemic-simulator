﻿using System;
#if COUNTERS
using System.Diagnostics;
#endif

namespace Corona
{
    public class StopwatchEx
    {
#if COUNTERS
        private readonly Stopwatch stopwatch = new Stopwatch();
        private int counter;
        private readonly int limit;
        private readonly Action onLimit;
#endif

        public StopwatchEx(int limit, Action onLimit)
        {
#if COUNTERS
            this.limit = limit;
            this.onLimit = onLimit;
#endif
        }

        public double Time
        {
            get;
#if COUNTERS
            private set;
#endif
        }

        public void Measure(Action action)
        {
#if COUNTERS
            stopwatch.Start();
#endif
            action();
#if COUNTERS
            stopwatch.Stop();
            counter++;
            if (counter % limit == 0)
            {
                Time = stopwatch.ElapsedMilliseconds / (double)limit;
                onLimit();
                stopwatch.Reset();
            }
#endif
        }
    }
}
