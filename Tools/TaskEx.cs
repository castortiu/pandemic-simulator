﻿using System;
using System.Threading.Tasks;

namespace Corona.Tools
{
    public class TaskEx
    {
        private Task updateLayoutTask;

        public void RunWhenNotRunning(Action action)
        {
            if (updateLayoutTask == null ||
                updateLayoutTask.Status == TaskStatus.Canceled ||
                updateLayoutTask.Status == TaskStatus.RanToCompletion ||
                updateLayoutTask.Status == TaskStatus.Faulted)
                updateLayoutTask = Tasks.Run(action);
        }
    }
}
