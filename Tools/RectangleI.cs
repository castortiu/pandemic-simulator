﻿namespace Corona
{
    public struct RectangleI
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
    }
}
