﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Corona.Annotations;
using Corona.Charts;
using Microsoft.Win32;

namespace Corona
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int frameCounter;
        private readonly Stopwatch stopwatch = Stopwatch.StartNew();
        private bool darkEnvironmentStyle;
        private int theme = -1;
        private readonly InfectionChart myChart;

        public MainWindow()
        {
            //Theme = Application.Current.Resources.MergedDictionaries[0];
            Engine.PropertyChanged += Engine_PropertyChanged;
            InitializeComponent();
            PlaygroundCanvas.Engine = Engine;
            PlaygroundCanvas.PlaygroundGrid = PlaygroundGrid;
            PlaygroundCanvas.PlaygroundScrollViewer = PlaygroundScrollViewer;
            PlaygroundGrid.Engine = Engine;
            PlaygroundGrid.Canvas = PlaygroundCanvas;
            PlaygroundGrid.MasterGrid = MasterGrid;
            PlaygroundGrid.PlaygroundScrollViewer = PlaygroundScrollViewer;
            PlaygroundScrollViewer.Engine = Engine;
            PlaygroundScrollViewer.PlaygroundCanvas = PlaygroundCanvas;
            PlaygroundScrollViewer.PlaygroundGrid = PlaygroundGrid;
            CompositionTarget.Rendering += Render;

            myChart = new InfectionChart { Engine = Engine };
        }

        public int FramesPerSecond { get; set; }

        public Engine Engine { get; } = new Engine();

        public bool DarkEnvironmentStyle
        {
            get => darkEnvironmentStyle;
            set
            {
                darkEnvironmentStyle = value;
                OnPropertyChanged(nameof(DarkEnvironmentStyle));
            }
        }

        public ResourceDictionary Theme
        {
            get
            {
                if (theme == -1)
                    return null;
                return Application.Current.Resources.MergedDictionaries[theme];
            }
        }

        private void ButtonStartSimulation_Click(object sender, RoutedEventArgs e)
        {
            Engine.StartSimulation();
        }

        private void ButtonStopSimulation_Click(object sender, RoutedEventArgs e)
        {
            Engine.StopSimulation();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Engine.Dispose();
            myChart.RequestClose();
            base.OnClosing(e);
        }

        protected void Render(object sender, EventArgs e)
        {
            frameCounter++;
            if (stopwatch.ElapsedTicks > 10000000)
            {
                FramesPerSecond = frameCounter;
                OnPropertyChanged(nameof(FramesPerSecond));
                frameCounter = 0;
                stopwatch.Restart();
            }
        }

        private void ButtonSetRules_Click(object sender, RoutedEventArgs e)
        {
            RulesWindow rules = new RulesWindow
            {
                Rules = Engine.Rules
            };

            if (rules.ShowDialog() == true)
            {
                Engine.Rules = rules.Rules;
            }
        }

        private void ButtonLoadRules_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Multiselect = false,
                Filter = Properties.Resources.VirusTypeWithExtension,
                CheckFileExists = true,
                AddExtension = true,
                DefaultExt = "vrs",
                Title = Properties.Resources.VirusType
            };
            if (openFileDialog.ShowDialog() == true)
            {
                Engine.Load(openFileDialog.FileName);
            }
        }

        private void ButtonSaveRules_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog openFileDialog = new SaveFileDialog
            {
                Filter = Properties.Resources.VirusTypeWithExtension,
                CheckFileExists = false,
                AddExtension = true,
                DefaultExt = "vrs",
                Title = Properties.Resources.VirusType
            };
            if (openFileDialog.ShowDialog() == true)
            {
                Engine.Save(openFileDialog.FileName);
            }
        }

        private void Engine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(e.PropertyName));
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ButtonHalfZoom_Click(object sender, RoutedEventArgs e)
        {
            SetZoom(PlaygroundGrid.Zoom * 0.5);
        }

        private void ButtonDoubleZoom_Click(object sender, RoutedEventArgs e)
        {
            SetZoom(PlaygroundGrid.Zoom * 1.2);
        }

        public void SetZoom(double zoom)
        {
            PlaygroundGrid.SetZoom(zoom, new Point(PlaygroundCanvas.Margin.Left + PlaygroundCanvas.Width, PlaygroundCanvas.Margin.Top + PlaygroundCanvas.Height));
        }

        private void ButtonSpeedLower_Click(object sender, RoutedEventArgs e)
        {
            Engine.ReduceSpeed();
        }

        private void ButtonSpeedBigger_Click(object sender, RoutedEventArgs e)
        {
            Engine.RaiseSpeed();
        }

        private void ButtonCreateSimulation_Click(object sender, RoutedEventArgs e)
        {
            Engine.CreateSimulation(() => { Application.Current.Dispatcher.Invoke(() => SetZoom(1)); });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Person? patientZeroNull = Engine?.World?.GetPatientZero();
            if (patientZeroNull == null)
                return;

            Person patientZero = patientZeroNull.Value;

            PlaygroundGrid.Visibility = Visibility.Visible;
            PatientZeroMarker.BringIntoView();

            ThicknessAnimation thicknessAnimation = new ThicknessAnimation
            {
                BeginTime = new TimeSpan(0),
                From = new Thickness((patientZero.PositionX + 1) * PlaygroundGrid.Zoom - 30, (patientZero.PositionY + 1) * PlaygroundGrid.Zoom - 30, 0, 0),
                To = new Thickness((patientZero.PositionX + 1) * PlaygroundGrid.Zoom, (patientZero.PositionY + 1) * PlaygroundGrid.Zoom, 0, 0),
                Duration = new Duration(TimeSpan.FromMilliseconds(200)),
                RepeatBehavior = new RepeatBehavior(3),
                AutoReverse = true
            };

            DoubleAnimation mySizeAnimation = new DoubleAnimation
            {
                From = 60,
                To = 0,
                Duration = new Duration(TimeSpan.FromMilliseconds(200)),
                RepeatBehavior = new RepeatBehavior(3),
                AutoReverse = true
            };
            mySizeAnimation.Completed += (o, args) => PlaygroundGrid.Visibility = Visibility.Hidden;

            PatientZeroMarker.BeginAnimation(Rectangle.MarginProperty, thicknessAnimation);
            PatientZeroMarker.BeginAnimation(Rectangle.WidthProperty, mySizeAnimation);
            PatientZeroMarker.BeginAnimation(Rectangle.HeightProperty, mySizeAnimation);
        }

        private void ThemeCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            theme = ThemeCheckbox.IsChecked == true ? 0 : -1;
            //PlaygroundCanvas.BackgroundColor = new IndexedColor(ThemeCheckbox.IsChecked == true ? Colors.White : Colors.Black);
            OnPropertyChanged(nameof(Theme));
        }

        private void MarkersCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            Engine.World.RenderMarkers = MarkersCheckbox.IsChecked == true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (myChart.IsVisible == false)
            {
                myChart.Owner = this;
                myChart.Show();
            }
            else
            {
                myChart.Hide();
            }
        }
    }
}
